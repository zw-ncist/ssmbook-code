package simulate.enhance;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
/*
* 借助JDK的动态代理，将SysLog接口实例中的逻辑功能，附着在原始的realObject的方法上。
* InvocationHandler中的invoke方法负责具体的“附着”逻辑，即如何安排SysLog接口中的方法与realObject原始方法之间的逻辑顺序。
* */
public class LoggableObjectFactory implements InvocationHandler {
    private Object realObject=null; //真实对象，具有基本的业务逻辑功能
    private SysLog sysLog =null;    //系统日志对象，具有日志记录功能
    private LoggableObjectFactory(Object realObject, SysLog sysLog){
            this.realObject=realObject;
            this.sysLog = sysLog;
    }
    /*
    * @Description: getEnhancedProxy 利用JDK动态代理创建一个代理对象
    * @Param: [realObject, sysLog] [真实的对象，Syslog接口实例]
    * @return: java.lang.Object 代理对象
    * @Author: zw
    * @Date: 2019/12/12 
    */
    public static Object getEnhancedProxy(Object realObject, SysLog sysLog){
        if(realObject!=null && sysLog !=null){
            //创建一个handler实例，这里是当前类的实例
            LoggableObjectFactory handler=new LoggableObjectFactory(realObject, sysLog);
            //针对传入的realObject创建代理，并使用handler进行功能的增强
            //并得到增强后的代理对象proxy，具备realObject真实对象的接口功能。
            return Proxy.newProxyInstance(realObject.getClass().getClassLoader(),realObject.getClass().getInterfaces(),handler);
        }else{
            return null;
        }
    }
    /*
    * @Description: invoke 将系统的日志功能与真实对象的业务功能进行整合，梳理执行顺序。
    * @Param: [proxy, method, args]
    * @return: java.lang.Object 原始对象的方法调用结果
    * @Author: zw
    * @Date: 2019/12/12
    */
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Object result=null;
        this.sysLog.before(method.getName()+" Params="+args[0].toString());
        try{
            result=method.invoke(this.realObject,args);//利用反射调用原始对象的方法
        }catch (Exception e){
            this.sysLog.catchSomething(e);
        }
        finally{
            this.sysLog.after(method.getName()+" Result="+result);
        }
        return result;
    }
}
