package simulate.enhance;
/*
* 定义了业务功能的接口
* */
public interface AuthorizationService {
    public Boolean LogIn(Integer userID);
    public Boolean LogOut(Integer userID);
}
