package simulate.enhance;
import java.io.IOException;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
/*
* 实现了系统中的日志功能
* */
public class SysLogImpl implements SysLog {
    public Logger logger=null;
    public SysLogImpl(String logerName,String logFilePath){
        if(logger==null){
            logger= Logger.getLogger(logerName);
            //文件控制器
            FileHandler fileHandler = null;
			try {
				fileHandler = new FileHandler(logFilePath);
		        SimpleFormatter formatter = new SimpleFormatter();  
		        fileHandler.setFormatter(formatter);  
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
            fileHandler.setLevel(Level.INFO); 
            logger.addHandler(fileHandler); 
        }
    }

    @Override
    public void before(String methodName) {
        logger.info("Before :"+methodName);
//        System.out.println("调用原始对象方法前....");
    }

    @Override
    public void after(String methodName) {
        logger.info("After :"+methodName);
//        System.out.println("....调用原始对象方法后");
    }

    @Override
    public void catchSomething(Exception e) {
        logger.warning("Catch Something..."+e.getStackTrace());
    }

}
