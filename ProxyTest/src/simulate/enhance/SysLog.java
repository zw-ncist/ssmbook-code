package simulate.enhance;
/*
 *定义系统中较为通用的日志功能接口
 */
public interface SysLog {
    public void before(String methodName);
    public void after(String methodName);
    public void catchSomething(Exception e);
}
