package simulate.mybatis;

import java.lang.reflect.Proxy;
/*
 * 模拟一个工厂类，用于创建代理对象
 * */
public class MapperProxyFactory<T> {


	@SuppressWarnings("unchecked")
	public T newInstance(MapperImpl<T> mapperImpl) {
		return (T) Proxy.newProxyInstance(mapperImpl.realMapper.getClassLoader(), new Class[] { mapperImpl.realMapper},
				mapperImpl);
	}
}
