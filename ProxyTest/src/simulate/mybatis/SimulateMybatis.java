package simulate.mybatis;

public class SimulateMybatis {

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MapperImpl userMapperImpl=new MapperImpl(UserMapper.class);
		MapperProxyFactory mpf=new MapperProxyFactory();
		UserMapper userMapperProxy=(UserMapper) mpf.newInstance(userMapperImpl);
		userMapperProxy.getAll();
	}

}
