package simulate.mybatis;

import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.util.Properties;
import java.util.ResourceBundle;

/*
 * 一个Mapper的通用实现类
 * */
public class MapperImpl<T> implements InvocationHandler {
	public Class<T> realMapper;// 保存T.calss，即接口类型的class

	public MapperImpl(Class<T> realMapper) {
		this.realMapper = realMapper;
	}

	/*
	 * 无论何时调用代理对象的方法，InvocationHandler的invoke方法都会被调用 因此在此处我们有机会调用真正对象的方法，并且增强它们
	 */
	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		// TODO Auto-generated method stub
		System.out.println("读取属性文件" + this.realMapper.getName().replace('.','/') + ".properties");
		System.out.println("找到方法名=" + method.getName());
		System.out.println("依据方法名，可从properties文件中获得相关的SQL语句，并执行！");
		System.out.println("sql语句="+getSql());
		return null;
	}

	private String getSql() {
		ResourceBundle resource=ResourceBundle.getBundle(this.realMapper.getName().replace('.','/'));
		return resource.getString("sql");
			
	}

}
