package com.company;

public class PrinterImpl implements Printer{
    @Override
    public void printInfo(Integer orderID) {
        System.out.println("("+this.getClass().toString()+")"+"I have printed orderID "+orderID+" in the Console!");
    }
}
