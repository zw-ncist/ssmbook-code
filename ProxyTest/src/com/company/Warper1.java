package com.company;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
//创建一个包装类，借助InvocationHandler的invoke方法，用于增强实际对象的功能
public class Warper1 implements InvocationHandler {
    private Object realObject=null;//用于存储真正的对象
    private String warperName;
    public Warper1(Object realObject, String warperName){
        this.warperName=warperName;
        this.realObject=realObject;
    }
    /*
    * 无论何时调用代理对象的方法，InvocationHandler的invoke方法都会被调用
    * 因此在此处我们有机会调用真正对象的方法，并且增强它们
    * */
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println(warperName+" Before ["+method.toString()+"] Invoke!");
        Object obj=method.invoke(this.realObject,args);
        System.out.println(warperName+" After ["+method.toString()+"] Invoke!");
        return obj;
    }
}
