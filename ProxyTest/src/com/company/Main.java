package com.company;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

public class Main {

    public static void main(String[] args) {

    /*
    *代理类可以在运行时被创建出来，可以实现指定的接口。
    *
    * 思路描述：
    * 1.定义一个接口和实现类:完成实际功能
    * 2.定义一个“包装类（调用处理器）”，并实现InvocationHandler接口：存储真实的对象realobject,并在invoke方法中对真实对象的方法进行增强。
    * 3.创建一个实际的对象realobject，并使用包装类对它进行包装:得到一个包装类实例warper
    * 4.利用Proxy.newProxyInstance，创建代理对象proxy：指定类加载器、指定代理类需要实现的接口、指定“包装类”的实例。
    * 5.当代理对象proxy中的方法被调用时，系统会调用包装类实例warper中的invoke方法
    * 6.当不断地对代理对象进行"包装"，并生成新的代理时，便得到了一种“链式结构”
    * */
        Printer p=new PrinterImpl();
        InvocationHandler warper1=new Warper1(p,"Warper1");//第一次对实际对象p进行包装
        Printer proxy1=(Printer)Proxy.newProxyInstance(Printer.class.getClassLoader(),new Class[]{Printer.class},warper1);

        //代理对象proxy1,具备Printer接口的功能，
        proxy1.printInfo(100);//会调用warper1.invoke方法
        System.out.println("*******************************");
        InvocationHandler warper2=new Warper2(proxy1,"Warper2");//第二次对得到的代理对象proxy1进行包装
        Printer proxy2=(Printer)Proxy.newProxyInstance(Printer.class.getClassLoader(),new Class[]{Printer.class},warper2);
        //代理对象proxy2，具备Printer接口的功能
        proxy2.printInfo(200);//此方法将进入Warper2.invoke方法
        //因此可以借助InvocationHandler中的invoke方法，不停对代理类proxy1,2,3..进行再包装。
        
        
    }
}
