SET SESSION FOREIGN_KEY_CHECKS=0;

/* Drop Tables */

DROP TABLE IF EXISTS task_student;
DROP TABLE IF EXISTS student;
DROP TABLE IF EXISTS major_class;
DROP TABLE IF EXISTS major;
DROP TABLE IF EXISTS teach_task;
DROP TABLE IF EXISTS teacher;
DROP TABLE IF EXISTS college;
DROP TABLE IF EXISTS course;




/* Create Tables */

CREATE TABLE college
(
	college_id bigint NOT NULL,
	college_name varchar(50) NOT NULL,
	PRIMARY KEY (college_id),
	UNIQUE (college_name)
);


CREATE TABLE course
(
	course_id bigint NOT NULL,
	course_name varchar(50) NOT NULL,
	course_score float NOT NULL,
	PRIMARY KEY (course_id)
);


CREATE TABLE major
(
	major_id bigint NOT NULL,
	major_name varchar(50) NOT NULL,
	college_id bigint NOT NULL,
	PRIMARY KEY (major_id),
	UNIQUE (major_name)
);


CREATE TABLE major_class
(
	class_id bigint NOT NULL,
	class_name varchar(50),
	major_id bigint NOT NULL,
	PRIMARY KEY (class_id)
);


-- 学生
CREATE TABLE student
(
	student_id bigint NOT NULL,
	name varchar(50) NOT NULL,
	student_number varchar(20) NOT NULL,
	class_id bigint NOT NULL,
	PRIMARY KEY (student_id),
	UNIQUE (student_number)
) COMMENT = '学生';


CREATE TABLE task_student
(
	task_id bigint NOT NULL,
	student_id bigint NOT NULL
);


CREATE TABLE teacher
(
	teacher_id bigint NOT NULL,
	teacher_name varchar(50) NOT NULL,
	college_id bigint NOT NULL,
	PRIMARY KEY (teacher_id),
	UNIQUE (teacher_name)
);


CREATE TABLE teach_task
(
	teacher_id bigint NOT NULL,
	course_id bigint NOT NULL,
	task_id bigint NOT NULL,
	PRIMARY KEY (task_id)
);



/* Create Foreign Keys */

ALTER TABLE major
	ADD FOREIGN KEY (college_id)
	REFERENCES college (college_id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE teacher
	ADD FOREIGN KEY (college_id)
	REFERENCES college (college_id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE teach_task
	ADD FOREIGN KEY (course_id)
	REFERENCES course (course_id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE major_class
	ADD FOREIGN KEY (major_id)
	REFERENCES major (major_id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE student
	ADD FOREIGN KEY (class_id)
	REFERENCES major_class (class_id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE task_student
	ADD FOREIGN KEY (student_id)
	REFERENCES student (student_id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE teach_task
	ADD FOREIGN KEY (teacher_id)
	REFERENCES teacher (teacher_id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE task_student
	ADD FOREIGN KEY (task_id)
	REFERENCES teach_task (task_id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;



