<%-- 
    Document   : registerForm
    Created on : 2016-11-1, 21:28:05
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%-- <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> --%>
<%-- <%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf"%> --%>
<%-- <%@ taglib uri="http://www.springframework.org/tags" prefix="s"%> --%>
<%@ page session="false"%>

<!-- 窗体名称 -->
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal"
		aria-hidden="true">×</button>
	<h4 class="modal-title" id="myModalLabel">新建一门课程</h4>
<!-- 	<s:message code="occam.uploadFiles.info" /> -->
</div>
<!-- 窗体主体 -->
<div class="modal-body form-horizontal">

<form id="myform" action="/ManageCourseServlet?act=addCourse" method="post">
课程名称: <input type="text" name="courseName">
课程学分: <input type="number" name="courseScore">

</form> 

</div>

<!-- 窗体下脚 -->
<div class="modal-footer">
	<button id="close" type="button" class="btn btn-default"
		data-dismiss="modal">关闭</button>
	<button id="submit" form="myform" type="button" class="btn btn-primary">提交更改</button>
</div>



