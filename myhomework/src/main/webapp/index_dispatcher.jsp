<%@page import="java.util.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%><html>
<body>
	<h2>Hello World!</h2>
	<%
	//设置作用域属性	
	Date d1 = Calendar.getInstance(Locale.CHINESE).getTime();
	Date d2 = Calendar.getInstance(Locale.US).getTime();
	request.setAttribute("request_attr", d1);	
	session.setAttribute("session_attr", d2);

	request.getRequestDispatcher("./mvc/readAttr/requestAttr").forward(request, response);
	%>
</body>
</html>
