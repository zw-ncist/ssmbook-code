<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Insert title here</title>
<link rel="stylesheet"
	href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
<script
	src="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>
<body>
${"Hello"}
<form name="myform" action="./mvc/upload/byRequestParam" method="post" enctype="multipart/form-data"> 
	userName： <input type="text" id="userName" name="userName" /><br />
	userPass： <input type="text" id="userPass" name="userPass" /><br />
	备注： <input type="text" id="note" name="note" /><br />
        请选择文件1： <input type="file" id="fileOne" name="file1" size="50" /> <br/>
        请选择文件2： <input type="file" id="fileTwo" name="file2" size="50" /> <br/>
             <input type="submit" value="上传_@RequestParam读取" /> 
             </form>
               <button id="button" name="">Ajax上传_@RequestPart读取</button>
             <hr/>

</body>

<script type="text/javascript">
$(function () {
    $("#button").click(function () {
    	formData = new FormData();
    	formName="myform"; 
    	formData.append("file", document.getElementById("fileOne").files[0]);
    	formData.append('properties', new Blob([JSON.stringify({
    	                "name": "root",
    	                "password": "root"                    
    	            })], {
    	                type: "application/json"
    	            }));
    	
        $.ajax({
            url: "./mvc/upload/byRequestPart",
            type: "post",
            contentType: false, //禁用contentType
            processData: false,// formData已经序列化
            dataType: "json",
            data: formData,
            success: function (response) {
                alert(response);
            },
            error: function () {

            }
        });

    });
})
</script>
</html>