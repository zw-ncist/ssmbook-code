<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
    <%@page isELIgnored="false" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<%java.util.Enumeration<String> request_atts=request.getAttributeNames();
	out.println("<h2>Request中的Attribute</h2>");
	while(request_atts.hasMoreElements()){
		String att_name=request_atts.nextElement();
		out.println(att_name+"="+request.getAttribute(att_name)+"<br/>");
	}
	%>
	<hr/>
	<%java.util.Enumeration<String> session_atts=request.getAttributeNames();
	out.println("<h2>Session中的Attribute</h2>");
	while(session_atts.hasMoreElements()){
		String att_name=session_atts.nextElement();
		out.println(att_name+"="+session.getAttribute(att_name)+"<br/>");
	}
	%>
	<hr/>
	<%java.util.Enumeration<String> app_atts=application.getAttributeNames();
	out.println("<h2>ServletContext中的Attribute</h2>");
	while(app_atts.hasMoreElements()){
		String att_name=app_atts.nextElement();
		out.println(att_name+"="+application.getAttribute(att_name)+"<br/>");
	}
	%>
</body>
</html>