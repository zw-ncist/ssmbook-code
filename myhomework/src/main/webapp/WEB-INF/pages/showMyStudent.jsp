<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@page isELIgnored="false" %>
    <%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<hr/>
<form:form modelAttribute="mystudent">  
    <table>  
        <tr>  
            <td>Name:</td><td><form:input path="name"/></td>  
            <td>birthday:</td><td><form:input path="birthday"/></td>  
        </tr>  
       
    </table>  
</form:form>  
<hr/>
mystudent=${requestScope.mystudent}<br/>
<hr/>
mystudent.name=${requestScope.mystudent.name}<br/>
<hr/>
mystudent.birthday=${requestScope.mystudent.birthday}<br/>
<hr/>
</body>
</html>