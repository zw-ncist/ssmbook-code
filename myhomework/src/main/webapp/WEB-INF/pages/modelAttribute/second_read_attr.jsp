<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@page isELIgnored="false" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

loginInfo=${requestScope.loginInfo}<br/>
info2=${requestScope.info2}<br/>
info3=${requestScope.info3}<br/>
(request作用域)info_session=${requestScope.info_session}<br/>
<hr/>
info_byPath=${requestScope.info_byPath}<br/>
info_byParam=${requestScope.info_byParam}<br/>
<hr/>
(session作用域)info_session=${sessionScope.info_session}<br/>

</body>
</html>