<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Insert title here</title>
<link rel="stylesheet"
	href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
<script
	src="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>
<body>
<form action="./mvc/upload/byRequestParam" method="post" enctype="multipart/form-data"> 
	userName： <input type="text" id="userName" name="userName" /><br />
	userPass： <input type="text" id="userPass" name="userPass" /><br />
	备注： <input type="text" id="note" name="note" /><br />
        请选择文件1： <input type="file" id="fileOne" name="file1"size="50" /> <br/>
        请选择文件2： <input type="file" id="fileTwo" name="file2"size="50" /> <br/>
	<input type="submit" value="普通提交表单_混合参数和文件_@RequestParam读取" /> 
 </form>
 <button id="Ajaxbutton" name="">Ajax提交_混合JSON对象和文件对象_@RequestPart读取</button>             
</body>

<script type="text/javascript">
$(function () {
    $("#Ajaxbutton").click(function () {
        //构建formData
        var formData = new FormData();
        //文件部分
        var fileOne = document.getElementById("fileOne").files[0];
        var fileTwo = document.getElementById("fileTwo").files[0];
        formData.append("fileOne", fileOne);
        formData.append("fileTwo", fileTwo);
        //json部分
        var loginInfo = JSON.stringify({
            "userName": $('#userName').val(),
            "userPass": $('#userPass').val()
        });
        //这里包装 可以直接转换成对象
        formData.append('loginInfo', new Blob([loginInfo], {type:"application/json"}));
        formData.append('note', $('#note').val());
        $.ajax({
            url: "./mvc/upload/byRequestPart",
            type: "post",
            contentType: false, //禁用contentType
            processData: false,// formData已经序列化
            dataType: "json",
            data: formData,
            success: function (data) {
                console.log(data);
                alert(JSON.stringify(data));
            }
        });
    });
})
</script>
</html>