package learn.springwebmvc;

import java.awt.Color;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;

@Configuration
public class IocConfig {
	@Bean(name = "mygreen")
	public Color getMyGreen() {
		return Color.green;
	}

	@Bean(name = "blueColor")
	public Color aBlueColor() {
		return new Color(0.0f, 0.0f, 1.0f);
	}


}
