package learn.springwebmvc;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
@Controller
public class TestDefaultHandlerExceptionResolver {
    @RequestMapping("/exceptionHandlerExceptionResolver/{num}") // http://localhost:8080/myhomework/mvc/exceptionHandlerExceptionResolver/ab
    @ResponseBody
	public Integer test(@PathVariable("num") Integer num) {
    	System.out.println(num);
		return num;
	}  
}
