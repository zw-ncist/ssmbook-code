package learn.springwebmvc;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

public class HandlerExceptionResolverA implements HandlerExceptionResolver {

	@Override
	public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object handler,
			Exception ex) {
		if (ex instanceof NumberFormatException) {//仅处理 NumberFormatException异常
			ModelAndView model = new ModelAndView("errorPage");
			model.addObject("info", "HandlerExceptionResolverA 解析器生成");
			model.addObject("error", ex);
			return model;
		} else {
			System.out.println("HandlerExceptionResolverA 忽略异常："+ex.getLocalizedMessage());
			return null;
		}
	}
}
