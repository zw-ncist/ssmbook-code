package learn.springwebmvc;

import java.util.ArrayList;
import java.util.List;

import javax.security.auth.message.callback.PrivateKeyCallback.Request;
import javax.servlet.http.HttpServletRequest;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/readParam")
@SessionAttributes(names= {"get_loginInfo_userName"},types= {LoginInfo.class})//注意是 或 的关系，只要符合的对象，一旦被加入到ModelAndView，则会被存至Session
public class ReadParamController {
	@RequestMapping("/loginForm_normal")
	public ModelAndView formParam(String userName, String userPass) {// 当请求参数与形参的名称一致时，不需要任何注解符号
		ModelAndView mv = new ModelAndView();
		mv.addObject("get_userName", userName).addObject("get_userPass", userPass).setViewName("showAllScope");
		return mv;
	}

	@RequestMapping(path = "/loginForm_normal", produces = MediaType.APPLICATION_JSON_VALUE) // 匹配Accept "application/json"
	@ResponseBody // 生成JSON响应
	public LoginInfo formParam_responseJSON(String userName, String userPass) {// 当请求参数与形参的名称一致时，不需要任何注解符号
		LoginInfo u = new LoginInfo();
		u.setUserName(userName);
		u.setUserPass(userPass);
		System.out.println(u);
		return u;// 会将对象转换为JSON
	}

	@RequestMapping(path = "/loginForm_normal", consumes = "application/json") // 匹配ContentType
	@ResponseBody // 生成JSON响应
	public LoginInfo formParam_requesJSON_responseJSON(@RequestBody LoginInfo loginInfo) {// 接受JSON数据，json与对象的属性名称一致时
		LoginInfo u = new LoginInfo();
		u.setUserName(loginInfo.getUserName() + "_控制器");
		u.setUserPass(loginInfo.getUserPass() + "_控制器");
		System.out.println(loginInfo);
		return u;// 会将对象转换为JSON
	}

	@PutMapping(path = "/loginForm_normal", consumes = "application/json") // 匹配ContentType,匹配Put方法
	@ResponseBody // 生成JSON响应
	public List<LoginInfo> formParam_requesJSONArray_responseJSON(@RequestBody List<LoginInfo> loginInfos) {// 接受JSON数据，json与对象的属性名称一致时
		List<LoginInfo> objects = new ArrayList<LoginInfo>();
		for (LoginInfo loginInfo : loginInfos) {
			LoginInfo u = new LoginInfo();
			u.setUserName(loginInfo.getUserName() + "_控制器");
			u.setUserPass(loginInfo.getUserPass() + "_控制器");
			objects.add(u);
		}

		System.out.println(objects);
		return objects;// 会将对象转换为JSONArray
	}

	@RequestMapping("/loginForm_normal_unmatch")
	public ModelAndView formParamUnmatch(@RequestParam("userName") String name, String userPass) {// 当请求参数与形参的名称不一致时，使用@RequestParam注解
		ModelAndView mv = new ModelAndView();
		mv.addObject("get_userName", name).addObject("get_userPass", userPass).setViewName("showAllScope");
		return mv;
	}

	@RequestMapping("/loginForm_bean")
	public ModelAndView formParamBean(LoginInfo loginInfo) {// 当请求参数与对象的属性名称一致时，不需要任何注解符号
		ModelAndView mv = new ModelAndView();
		mv.addObject(loginInfo).setViewName("showAllScope");//loginInfo会被放在Session中
		return mv;
	}

	@RequestMapping("/loginURL_path2/{userName}/{userPass}")
	public ModelAndView urlPathParam2(String userName, String userPass) {// 不使用@PathVariable注解符号，无效，即使名字相同.(也可以在Java8中编译时，加入debugging information或使用-parameters编译标记)
		ModelAndView mv = new ModelAndView();
		mv.addObject("get_userName", userName).addObject("get_userPass", userPass).setViewName("showAllScope");
		System.out.println("=============" + userName + userPass);// nullnull
		return mv;
	}

	@RequestMapping("/loginURL_path/{userName}/{userPass}")
	public ModelAndView urlPathParam(@PathVariable("userName") String userName,
			@PathVariable("userPass") String userPass) {// 使用@PathVariable注解符号
		ModelAndView mv = new ModelAndView();
		mv.addObject("get_loginInfo_userName", userName).addObject("get_userPass", userPass).setViewName("showAllScope");
		//get_loginInfo_userName会被放在Session中，因为get_loginInfo_userName
		return mv;
	}

	@RequestMapping("/loginURL_path_bean/{userName}/{userPass}")
	public ModelAndView urlPathParamBean(LoginInfo myLoginInfo) {// 当路径参数与对象的属性名称一致时，不需要任何注解符号(此时使用@ModelAttribute注解参数也可以)
		ModelAndView mv = new ModelAndView();
		mv.addObject(myLoginInfo).setViewName("showAllScope");//loginInfo会被放在Session中,因为types={LoginInfo.class}
		return mv;
	}
	
	@RequestMapping("/login_post")//Post方式，@RequestParam正常使用
	@ResponseBody
	public String test(@RequestParam("userName") String param_name,HttpServletRequest request) {
		return param_name+"  "+request.getParameter("userName");
	}

}
