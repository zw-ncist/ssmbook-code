package learn.springwebmvc;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/interceptor")
public class TestInterceptor {
@RequestMapping("/login")
public String login(LoginInfo li,HttpSession session) {// http://localhost:8080/myhomework/mvc/interceptor/login
	System.out.println("执行控制器中处理方法login "+System.currentTimeMillis());
	if(session.getAttribute("loginInfo")!=null) {
		return "success";
	}
	if(li.getUserName().equals("张三")&&li.getUserPass().equals("123")) {
		session.setAttribute("loginInfo", li);//验证成功,保存到session
		return "success";//登录成功，服务器内部转发到"/WEB-INF/pages/success.jsp"
	}else {
		return "redirect:../../loginForm.jsp";//让浏览器重新发送请求所需要的URL
		}
}
}
