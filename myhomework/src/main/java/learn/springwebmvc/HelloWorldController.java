package learn.springwebmvc;

import java.awt.Color;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/hello")
public class HelloWorldController {
	@Autowired
	@Qualifier("blueColor")
	Color mycolor;
	
	public Color getMycolor() {
		return mycolor;
	}

	public void setMycolor(Color mycolor) {
		this.mycolor = mycolor;
	}

	@RequestMapping("/world")
	public ModelAndView sayHello() {
		ModelAndView mv=new ModelAndView();
		mv.addObject("info", "Hello World!");
		mv.addObject("obj",mycolor);
		mv.setViewName("helloWorld");
		
		return mv;
	}
}