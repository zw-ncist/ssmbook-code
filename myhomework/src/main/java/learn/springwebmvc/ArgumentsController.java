package learn.springwebmvc;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Writer;
import java.net.URI;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.MatrixVariable;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.UriComponentsBuilder;

@Controller
@RequestMapping("/agrguments")
public class ArgumentsController {
	@RequestMapping("/webRequest")
	@ResponseBody
	public String handler_WebRequest(WebRequest wr, NativeWebRequest nwr) {// 使用web请求的通用接口
		String l = wr.getSessionId();// 获得SessionID
		HttpServletRequest hsr = (HttpServletRequest) nwr.getNativeRequest();// 获得实际的HttpServletRequest对象
		String s = l + "," + hsr.getContextPath();
		return s;

	}

	@RequestMapping("/servlet_Request_Response")
	@ResponseBody
	public String handler_ServletRequest(HttpServletRequest request, HttpServletResponse response) {// 使用实际的ServletReqest、ServletResponse或它们的子类型对象
		return request.getPathInfo() + "," + response.getCharacterEncoding();
	}

	@RequestMapping("/session")
	@ResponseBody
	public String handler_session(HttpSession session) {// 使用实际的Session对象
		session.setAttribute("time", new Date());
		return session.getAttribute("time").toString();
	}

	@RequestMapping("/method")
	@ResponseBody
	public String handler_method(HttpMethod method) {//

		return method.toString();
	}

	@RequestMapping("/locale_timeZone")
	@ResponseBody
	public String handler_locale(Locale locale, TimeZone timeZone, ZoneId zoneId) {
		// 地区和时区等信息，由LocaleContextResolver确定
		return locale.getCountry() + "," + timeZone.toString() + "," + zoneId.getId();
	}

	@RequestMapping(path = "/input_output", method = RequestMethod.POST)
	public void handler_locale(InputStream input, Writer writer) throws Exception {// 用loginForm提交
		// ServletAPI中请求和响应中原始输入和输出对象。
		StringBuffer sb = new StringBuffer();
		BufferedReader br = new BufferedReader(new InputStreamReader(input, "utf-8")); // 直接从输入对象读取信息
		String s;
		while ((s = br.readLine()) != null) {
			sb.append(s);
		}
		String str = sb.toString();
		writer.write(str);// 直接向输出对象写入内容

	}

	@RequestMapping("/pathVariable/{userId}")
	@ResponseBody
	public String handler_pathVariable(@PathVariable("userId") long id) {
		// 获取路径参数作为参数的值
		return "userId=" + id;
	}

	@RequestMapping("/matrixVariable/{userId}") // http://localhost:8080/myhomework/mvc/agrguments/matrixVariable/153;name=test;age=22
	@ResponseBody
	public String handler_matrixVariable(@PathVariable("userId") long id, @MatrixVariable String name,
			@MatrixVariable int age) {
		// 获取路径参数作为参数的值
		// 获取矩阵变量，转换后赋值给name，age形参
		return "userId=" + id + " name=" + name + " age" + age;
	}

	@RequestMapping("/matrixVariable/{userId}/{roleId}") //http://localhost:8080/myhomework/mvc/agrguments/matrixVariable/153;name=test;age=22/386;name=admin;dep=HR
	@ResponseBody
	public String handler_matrixVariable2(@PathVariable("userId") long uid, @PathVariable("roleId") long rid,
			@MatrixVariable(name = "name", pathVar = "userId") String name,//获取指定路径的矩阵变量
			@MatrixVariable(name = "age", pathVar = "userId") int age,
			@MatrixVariable(name = "name", pathVar = "roleId") String role_name,//不同路径的矩阵变量可以重名
			@MatrixVariable(name = "dep", pathVar = "roleId") String department, @MatrixVariable MultiValueMap<String, Object> matrixVars,
	        @MatrixVariable(pathVar="roleId") MultiValueMap<String, String> roleMatrixVars) {
		System.out.println("<"+uid+","+rid+">"+name+"-"+age+"-"+role_name+"-"+department);
		System.out.println(matrixVars);
		System.out.println(roleMatrixVars);
		return "<"+uid+","+rid+">"+name+"-"+age+"-"+role_name+"-"+department;
	}

	@RequestMapping("/requestParam")//http://localhost:8080/myhomework/mvc/agrguments/requestParam?userName=Tom&userPass=123
	@ResponseBody
	public String formParamUnmatch(@RequestParam("userName") String name, String userPass) {// 当请求参数与形参的名称不一致时，使用@RequestParam注解
		return "userName=" + name + " userPass=" + userPass;
	}
	@RequestMapping("/header_cookie")// http://localhost:8080/myhomework/mvc/agrguments/header_cookie
	@ResponseBody
	public String handler_header_cookie(@RequestHeader("Accept") String accept,@CookieValue("JSESSIONID") String sessionId) {
		//将请求头中的Accept赋值给参数accept
		//将名为JSESSIONID的Cookie的值赋值给参数sessionId
		return "Header Accept=" + accept+ ";;CookieValue=" + sessionId;
	}
	
	@RequestMapping("/requestBody")// http://localhost:8080/myhomework/mvc/agrguments/requestBody
	@ResponseBody
	public String handler_requestBody(@RequestBody LoginInfo info) {
		//将请求体转换为LoginInfo类型的对象，赋值给形参info
		System.out.println(info.toString());
		return info.toString();
	}
	@RequestMapping("/httpEntity")// http://localhost:8080/myhomework/mvc/agrguments/httpEntity
	@ResponseBody
	public String handler_httpEntity(HttpEntity<LoginInfo> entity) {
		LoginInfo info=entity.getBody();//获取请求体
		HttpHeaders headers=entity.getHeaders();//获取请求头
		List<MediaType> accept=headers.getAccept();
		//将请求体转换为LoginInfo类型的对象，赋值给形参info
		System.out.println(entity.toString());
		System.out.println(info.toString());
		System.out.println(accept.toString());
		return entity.toString();
	}
	
	@RequestMapping("/model")// http://localhost:8080/myhomework/mvc/agrguments/model
	public String handler_model(Map<String,Object> map,Model model,ModelMap modelMap) {
		map.put("info0", "通过Map添加");
		model.addAttribute("info1", "通过Model添加");
		modelMap.addAttribute("info2", "通过ModelMap添加");
		return "showModel";//访问/WEB-INF/pages/showModel.jsp
	}
	
	@RequestMapping("/redirect")
	public String redirectTo(RedirectAttributes ra) {
		ra.addAttribute("userName", "Tom");//设置Attribute属性，会出现在查询字符串中.不会放在request作用域中
		ra.addFlashAttribute("userPass", "123456789");//设置Flash Attribute，会放在重新向后的request的作用域中
		return "redirect:redirect_result";//redirect: 开头表明了一个重定向
	}

	@RequestMapping("/redirect_result")//重定向的目标
	public String redirect_result(@RequestParam("userName") String name) {
		System.out.println("redirect传过来的请求参数  userName ="+name);
		return "result_redirect";//访问/WEB-INF/pages/result_redirect.jsp
	}
	
	@RequestMapping("/set_request_session_attr")//设置request、session、model的属性
	//http://localhost:8080/myhomework/mvc/agrguments/set_request_session_attr
	public String set_request_session_model_attr(HttpServletRequest request,HttpSession session) {
		//设置作用域属性	
		String d1="Request";
		String d2="Session";
		request.setAttribute("request_attr", d1);	
		session.setAttribute("session_attr", d2);
		System.out.println(d1+"-"+d2);
		return "forward:read_request_session_Attr";//表明这是一个转发，不经过视图解析器
	}
	
	@RequestMapping("/read_request_session_Attr")//使用注解读取已有的指定作用域的属性，
	@ResponseBody
	public String readrequest_session_Attr(@RequestAttribute("request_attr") String d1,@SessionAttribute String session_attr) {
		//当作用域内的属性名称与形参名称一致时，可以不用指定属性名称
		String s=d1+"||"+session_attr;
		System.out.println(s);
		return s;
	}
	
	@RequestMapping("/modelAttr_set")// http://localhost:8080/myhomework/mvc/agrguments/modelAttr_set
	public String modelAttr_set(HttpSession session,Model m) {//形参中给出Model对象
		m.addAttribute("model_attr","from Model!");
		session.setAttribute("session_attr", "from Session!");
		return "forward:modelAttr_get";//表明这是一个转发，不经过视图解析器
	}
	@RequestMapping("/modelAttr_get")//使用注解读取已有的指定作用域的属性，
	@ResponseBody
	public String modelAttr_set(@ModelAttribute("model_attr") String model_attr) {
		//当作用域内的属性名称与形参名称一致时，可以不用指定属性名称
		String s=model_attr;
		System.out.println(s);
		return s;
	}
	@RequestMapping("/uriComponentsBuilder")// http://localhost:8080/myhomework/mvc/agrguments/uriComponentsBuilder
	@ResponseBody
	public String modelAttr_set(UriComponentsBuilder uri) {//形参中给出Model对象

		URI uri1=uri.build().encode().toUri();//得到当前Servlet 映射下的路径 http://localhost:8080/myhomework/mvc
		URI uri2=uri.path("/addedPath").build().encode().toUri();//增加了一个路径
		URI uri3=uri.queryParam("name", "Tom").queryParam("pass", "{pass}").build().expand("123456").encode().toUri();//添加了两个参数，并使用了变量的替换
		System.out.println(uri1.toString());
		System.out.println(uri2.toString());
		System.out.println(uri3.toString());
	
		return uri3.toString();//
	}
	

	
}
