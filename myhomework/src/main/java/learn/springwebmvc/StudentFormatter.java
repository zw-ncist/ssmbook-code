package learn.springwebmvc;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.springframework.format.Formatter;

public class StudentFormatter implements Formatter<Student> {

	@Override
	public String print(Student object, Locale locale) {//将Student对象进行格式化输出
		 //创建SimpleDateFormat对象，指定样式 
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy年MM月dd日");
		return String.format(locale, "学生姓名:%s, 学生出生日期:%s", object.getName(), sdf.format(object.getBirthday()));
	}

	@Override
	public Student parse(String text, Locale locale) throws ParseException {// 输入 n学生姓名，b生日 ，解析为Student对象
		String[] parts = text.split(",");
		//解析姓名
		Student stu = new Student();
		stu.setName(parts[0].substring(1));
		//解析生日
		Date d = new SimpleDateFormat("dd-MM-yyyy").parse(parts[1].substring(1));// 提交的数据为
																										// 15-08-2021
		stu.setBirthday(d);
		return stu;
	}

}
