package learn.springwebmvc;

import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/redirect_forward")
public class RedirectForwardController {
	@RequestMapping("/redirect")//http://localhost:8080/myhomework/mvc/redirect_forward/redirect
	public String redirectTo(RedirectAttributes ra) {
		ra.addAttribute("userName", "Tom");//设置Attribute属性，会出现在查询字符串中
		ra.addFlashAttribute("userPass", "123456789");//设置Flash Attribute，会放在重新向后的request的作用域中
		return "redirect:result";
	}

	@RequestMapping("/result")
	public String result(@RequestParam("userName") String name) {
		System.out.println("redirect传过来的请求参数  userName ="+name);
		return "result_redirect";
	}
}
