package learn.springwebmvc;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;

public class LoginInfoConverter extends AbstractHttpMessageConverter<LoginInfo> {

	public LoginInfoConverter() {
		super(new MediaType[] {new MediaType("text", "loginInfo"),MediaType.TEXT_PLAIN});
		//当前HttpMessageConverter的支持MIME类型。   text/loginInfo 和  text/plain 
		this.setDefaultCharset(Charset.forName("utf-8"));//设置默认字符集
		System.out.println("LoginInfoConverter支持的MIME" + this.getSupportedMediaTypes().toString());
	}

	@Override
	protected boolean supports(Class<?> clazz) {
		return LoginInfo.class == clazz;//仅支持LoginInfo类
	}

	@Override
	protected LoginInfo readInternal(Class<? extends LoginInfo> clazz, HttpInputMessage inputMessage)
			throws IOException, HttpMessageNotReadableException {
		InputStream input = inputMessage.getBody(); // 获取输入流
		byte b[] = new byte[1024]; // 接收数组
		int len = 0;
		int temp = 0; // 接收数据
		while ((temp = input.read()) != -1) {//循环读取数据
			b[len] = (byte) temp;
			len++;
		}
		input.close(); // 关闭输出流
		String request = new String(b, 0, len);//变为字符串，形如 userName=%E7%8E%8B%E4%BA%94&userPass=123456
		String[] paramValues = request.split("&");//进行简单的拆分		
		String name = URLDecoder.decode(paramValues[0].split("=")[1], "utf-8");//解码
		String pass = URLDecoder.decode(paramValues[1].split("=")[1], "utf-8");	
		LoginInfo li = new LoginInfo();//创建转换后对象
		li.setUserName(name);
		li.setUserPass(pass);
		System.out.println("用户提交内容" + new String(b, 0, len)); // 把byte数组变为字符串输出
		return li;
	}

	@Override
	protected void writeInternal(LoginInfo t, HttpOutputMessage outputMessage)
			throws IOException, HttpMessageNotWritableException {
	          OutputStream outputStream = outputMessage.getBody();
	           String body ="用户名=" +t.getUserName() + ",密码=" + t.getUserPass() + "！";
	          System.out.println("write="+body);
	          outputStream.write(body.getBytes("utf-8"));
	          outputStream.close();
	}

}
