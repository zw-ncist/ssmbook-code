package learn.springwebmvc;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class MyInterceptor implements HandlerInterceptor {
	private String value;
	
	public MyInterceptor(String value) {
		this.value = value;
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		Thread.currentThread();//为了看出执行顺序，有意让线程睡眠1毫秒
		Thread.sleep(1) ;
		System.out.println("执行preHandle"+this.value+" "+System.currentTimeMillis());
		request.setAttribute("preHandleTime", System.currentTimeMillis());//设置request作用域的属性
		String name = request.getParameter("userName");//获取请求参数
		String pass = request.getParameter("userPass");

		LoginInfo li = (LoginInfo) request.getSession().getAttribute("loginInfo");//获取session作用域中的已有对象
		if (li != null) {//session中已存在指定对象，表明用户已经登录
			return true;
		} else if (name != null && pass != null) {//请求中包含了两个参数
			if (name.trim().length() != 0 && pass.trim().length() != 0) {//两个参数均不为空串
				return true;
			} else {
				response.sendRedirect(request.getContextPath() + "/loginForm.jsp");//参数有空串，直接重定向
				System.out.println("preHandle"+this.value+" 返回false "+System.currentTimeMillis());
				return false;
			}

		} else {//用户没登陆，且没有传递必要的两个参数，直接重定向
			response.sendRedirect(request.getContextPath() + "/loginForm.jsp");
			System.out.println("preHandle"+this.value+" 返回false "+System.currentTimeMillis());
			return false;
		}

	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		Thread.currentThread();
		Thread.sleep(1) ;
		System.out.println("执行postHandle"+this.value+" "+System.currentTimeMillis());
		modelAndView.addObject("postHandleTime", System.currentTimeMillis());
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		Thread.currentThread();
		Thread.sleep(1) ;
		System.out.println("执行afterCompletion"+this.value+" "+System.currentTimeMillis());
	}

}
