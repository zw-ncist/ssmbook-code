package learn.springwebmvc;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.http.HttpStatus;
@Controller
@RequestMapping("/exceptionHandlerExceptionResolver")
public class TestExceptionHandlerExceptionResolver {

	@ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public String exHandler1 (Exception ex) {
        return ex.getLocalizedMessage();
    }

    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ExceptionHandler(MethodArgumentTypeMismatchException .class)
    public String exHandler2 (MethodArgumentTypeMismatchException  error, Model model) {
        model.addAttribute("error", error);
        return "errorPage";
    }
    @RequestMapping("/ex1/{num}") // http://localhost:8080/myhomework/mvc/exceptionHandlerExceptionResolver/ex1/ab
    @ResponseBody
	public Integer test(@PathVariable("num") Integer num) {
    	System.out.println(num);
    	if(num>100) {// http://localhost:8080/myhomework/mvc/exceptionHandlerExceptionResolver/ex1/1001
    		throw new NumberFormatException("����̫���ˣ�");
    	}
		return num;
	}
   
    
}
