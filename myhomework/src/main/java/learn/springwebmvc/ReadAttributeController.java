package learn.springwebmvc;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/readAttr")
public class ReadAttributeController {
	@RequestMapping("/requestAttr")
	public ModelAndView readRequestAttr(@RequestAttribute("request_attr") Date d1,@SessionAttribute Date session_attr,ModelAndView mv) {
		
		mv.addObject("mv_from_request", d1);//加入到ModelAndView
		mv.addObject("mv_from_session", session_attr);//加入到ModelAndView
		mv.setViewName("showAttr");//默认做请求转发
		return mv;
	}
}
