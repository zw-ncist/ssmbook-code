package learn.springwebmvc;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/sessionAttributes")
@SessionAttributes(names = {"loginInfo"}, types = { LoginInfo.class }) // 注意是 或 的关系，只要符合的对象，一旦被加入到Model，则会被存至Session
public class Test_SessionAttributes_Controller {
	@RequestMapping("set_attr_byname")//http://localhost:8080/myhomework/mvc/sessionAttributes/set_attr_byname
	public String set_attr_byname(Model m) {//形参列表加入Model对象
		LoginInfo info=new LoginInfo();
		info.setUserName("Tom");
		info.setUserPass("123");
		m.addAttribute("loginInfo", info);//属性名称匹配
		return "forward:set_attr_byclass";//做个转发
	}
	@RequestMapping("set_attr_byclass")
	public String set_attr_byclass(Model m) {//形参列表加入Model对象
		LoginInfo myinfo=new LoginInfo();
		myinfo.setUserName("Jerry");
		myinfo.setUserPass("789");
		m.addAttribute("myinfo",myinfo);//类型匹配
		return "forward:read_attr";
	}
	@RequestMapping("read_attr")
	public String read_attr(HttpSession session,SessionStatus status) {//读取由@SessionAttributes存储的属性
		String s=session.getAttribute("loginInfo")+","+session.getAttribute("myinfo");
		System.out.println("第一次读取="+s);
		status.setComplete();//清除session中的属性值。
		return "secondReadSession";//访问web-inf/pages/secondRead.jsp,应该什么都读不出来了。
	}
}
