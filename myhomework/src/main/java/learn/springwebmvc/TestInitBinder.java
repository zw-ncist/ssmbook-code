package learn.springwebmvc;

import java.beans.PropertyEditorSupport;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.format.Formatter;
import org.springframework.format.datetime.DateFormatter;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("initBinder")
public class TestInitBinder {

	@InitBinder // 不指定'value'值，那么在每个HTTP请求上都会调用@InitBinder注释的方法。
	public void initBinder1(WebDataBinder binder) {//
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");// 提交的数据为 15-08-2021
		dateFormat.setLenient(false);
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));// 适用于Date类型
	}
	@InitBinder("mystudent") // 使用value值，可以针对特定的对象应用这个被注释的方法。
	public void initBinder2(WebDataBinder binder) {
		binder.registerCustomEditor(String.class, "name", new PropertyEditorSupport() {// 指定应用于 name属性
			@Override
			public String getAsText() {
				System.out.println("调用initBinder2：mystudent.name,getAsText");
				String origianlName = (String) getValue();
				return origianlName.substring(0, origianlName.length() - 1) + "*";// 作为字符串返回时(主要是为了便于人类阅读)调用。本例中是在控件上显示,所以省去一个字。
			}
			@Override
			public void setAsText(String text) throws IllegalArgumentException {
				System.out.println("调用initBinder2：mystudent.name,setAsText");
				String newValue = text.trim().replaceAll("[a-zA-Z]", "");// 将文本转换为属性值时，必须去掉所有英文字母
				setValue(newValue);
			}
		});
	}

	@RequestMapping("/testInitBinder1") // http://localhost:8080/myhomework/mvc/initBinder/testInitBinder1?name=张三ab&birthday=15-08-2021
	public String testInitBinder1(@ModelAttribute("mystudent") Student student) {
		System.out.println(student);
		return "showMyStudent";
	}

}
