package learn.springwebmvc;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.http.HttpStatus;
@Controller
public class TestResponseStatusExceptionResolver {
	@RequestMapping("/responseStatusExceptionResolver")   
	public String test(String name) throws Exception,InvalidName {
    	if(name.equals("admin")) {// http://localhost:8080/myhomework/mvc/responseStatusExceptionResolver?name=admin
    		throw new InvalidName("非法用户名A："+name); //抛出自定义异常
    	}
    	if(name.equals("administrator")) {// http://localhost:8080/myhomework/mvc/responseStatusExceptionResolver?name=administrator
    		throw new Exception(new InvalidName("非法用户名B："+name)); //包裹了自定义异常
    	}
		return name;
	}
}
