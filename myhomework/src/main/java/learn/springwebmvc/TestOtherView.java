package learn.springwebmvc;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/showOtherView")
public class TestOtherView {
	@RequestMapping("/excel")//http://localhost:8080/myhomework/mvc/showOtherView/excel
	public String excel(Model m) {
		List<LoginInfo> list=new ArrayList<LoginInfo>();
		Random r=new Random();
		
		for(int i=0;i<10;i++) {
			LoginInfo l=new LoginInfo();
			l.setUserName("用户"+i);
			l.setUserPass(""+r.nextInt());
			list.add(l);
		}
		m.addAttribute("loginInfos", list);
		
		return "excelView";//返回Excel视图的bean名称
	}
}
