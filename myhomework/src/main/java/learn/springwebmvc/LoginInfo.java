package learn.springwebmvc;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement // 注意xml根元素的大小写问题，数据应当形如
/*
 * <?xml version="1.0" encoding="UTF-8"?> 
 * <loginInfo> 
 * <userName>张三</userName>
 * <userPass>123</userPass>
 * </loginInfo>
 */
public class LoginInfo {
	private String userName;
	private String userPass;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserPass() {
		return userPass;
	}

	public void setUserPass(String userPass) {
		this.userPass = userPass;
	}

	@Override
	public String toString() {
		return "LoginInfo [userName=" + userName + ", userPass=" + userPass + "]";
	}

}
