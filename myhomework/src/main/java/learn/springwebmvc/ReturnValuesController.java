package learn.springwebmvc;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/returns")
public class ReturnValuesController {

	@RequestMapping("/responseBody") // http://localhost:8080/myhomework/mvc/returns/responseBody
	@ResponseBody
	public LoginInfo responseBody() {
		LoginInfo loginInfo = new LoginInfo();
		loginInfo.setUserName("用户名");
		loginInfo.setUserPass("登录密码");
		return loginInfo;
	}
	
	@RequestMapping("/modelAttribute") // http://localhost:8080/myhomework/mvc/returns/modelAttribute
	public @ModelAttribute LoginInfo modelAttribute() {//通过RequestToViewNameTranslator隐式地确定视图名,所以会转发到/WEB-INF/pages/returns/modelAttribute.jsp
		LoginInfo loginInfo = new LoginInfo();
		loginInfo.setUserName("用户名");
		loginInfo.setUserPass("登录密码");
		return loginInfo;
	}
	

	@GetMapping("/responseEntity") //http://localhost:8080/myhomework/mvc/returns/responseEntity
	public ResponseEntity<LoginInfo> responseEntity() {//ResposneEntity继承自HttpEnity，并增加了 状态码
		LoginInfo loginInfo = new LoginInfo();
		loginInfo.setUserName("用户名");
		loginInfo.setUserPass("登录密码");
		  return ResponseEntity.status(HttpStatus.FOUND)
			         .header("My-Header", "myHeader")
			         .body(loginInfo);
	}
	
	@GetMapping("/httpEntity") //http://localhost:8080/myhomework/mvc/returns/httpEntity
	public HttpEntity<LocalDateTime> httpEntity() {//HttpEntity包括 头和体
		// LocalDate + LocalTime
		LocalDateTime ldt = LocalDateTime.of(LocalDate.now(), LocalTime.now());
			HttpHeaders headers = new HttpHeaders();
	        headers.put("Server", Arrays.asList("Kevin's Private Server"));
	        HttpEntity<LocalDateTime> responseEntity = new HttpEntity<LocalDateTime> (ldt, headers);
		  return responseEntity;
	}
	
	@GetMapping("/httpHeaders") //http://localhost:8080/myhomework/mvc/returns/httpHeaders
	public HttpHeaders httpHeaders() {//返回一个不含响应体，只含头部的响应
		HttpHeaders headers = new HttpHeaders();		
        headers.put("Refresh", Arrays.asList("10; URL=http://localhost:8080/myhomework/mvc/returns/httpEntity"));//10秒后向URL发送请求
		return headers;
	}
	@GetMapping("/map") //http://localhost:8080/myhomework/mvc/returns/map
	public Map map() {//返回一个Map,会被添加到隐式模型的属性，并通过RequestToViewNameTranslator隐式地确定视图名,所以会转发到/WEB-INF/pages/returns/map.jsp
		Map m=new HashMap<String,Object>();
		LoginInfo loginInfo = new LoginInfo();
		loginInfo.setUserName("用户名 from Map");
		loginInfo.setUserPass("登录密码 from Map");
		m.put("myinfo", loginInfo);
		return m;
	}
	@GetMapping("/model") //http://localhost:8080/myhomework/mvc/returns/model
	public Model model() {//返回一个Model,会被添加到隐式模型的属性，并通过RequestToViewNameTranslator隐式地确定视图名,所以会转发到/WEB-INF/pages/returns/map.jsp
		Model model=new ExtendedModelMap();
		LoginInfo loginInfo = new LoginInfo();
		loginInfo.setUserName("用户名 from Model");
		loginInfo.setUserPass("登录密码 from Model");
		model.addAttribute("myinfo", loginInfo);
		return model;
	}
}	
