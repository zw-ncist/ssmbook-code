package learn.springwebmvc;

import javax.servlet.Filter;
import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration.Dynamic;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;
import org.springframework.web.servlet.FrameworkServlet;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class MyWebAppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {
	@Override
	protected Class<?>[] getRootConfigClasses() {
		//Spring IoC容器的配置类
		return  new Class[] {IocConfig.class};
	}

	@Override
	protected Class<?>[] getServletConfigClasses() {
		//DispatcherServlet的配置类
		return new Class[] {MvcConfig.class};
	}

	@Override
	protected String[] getServletMappings() {
		//DispatcherServlet的映射路径
		return new String[] {"/mvc/*"};
	}

	@Override
	protected void customizeRegistration(Dynamic registration) {
		//配置Multipart
		//uploadFiles 目录，文件大小为10M，整个请求不超过50M
		long M=(long)(Math.pow(2, 20));
		long singleFileMax=10*M;
		long totalFileMax=50*M;
        registration.setMultipartConfig(new MultipartConfigElement("d:\\uploadFiles",singleFileMax,totalFileMax,0));

	}

	@Override
	protected Filter[] getServletFilters() {
		CharacterEncodingFilter filter=new CharacterEncodingFilter();//配置一个过滤器进行转码
		filter.setEncoding("UTF-8");
		return new Filter[] {filter};
	}






	
	

}
