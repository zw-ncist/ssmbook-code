package learn.springwebmvc;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/handlerExceptionResolverComposite") 
public class TestHandlerExceptionResolverComposite {
	@RequestMapping("/ex1") // http://localhost:8080/myhomework/mvc/handlerExceptionResolverComposite/ex1?name=admin
    @ResponseBody
	public String test1(String name) throws InvalidName  {
    	if(name.equals("admin")) {
    		throw new InvalidName ("无效名称");
    	}
		return name;
	}  
	@RequestMapping("/ex2") 
    @ResponseBody
	public String test2(String ex) throws Exception {
    	if(ex.equals("NumberFormatException")) {// http://localhost:8080/myhomework/mvc/handlerExceptionResolverComposite/ex2?ex=NumberFormatException
    		
    		throw new NumberFormatException("数字格式异常");
    	}
    	if(ex.equals("NoSuchMethodException")) {// http://localhost:8080/myhomework/mvc/handlerExceptionResolverComposite/ex2?ex=NoSuchMethodException
    		throw new NoSuchMethodException("无此方法异常");
    	}
		return ex;
	}  
}
