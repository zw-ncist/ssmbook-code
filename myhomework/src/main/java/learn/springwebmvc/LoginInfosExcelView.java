package learn.springwebmvc;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.core.Ordered;
import org.springframework.web.servlet.view.document.AbstractXlsView;

public class LoginInfosExcelView extends AbstractXlsView {

	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		List<LoginInfo> list = (List<LoginInfo>) model.get("loginInfos");
		Sheet sheetA = workbook.createSheet("第一个sheet");
		sheetA.setFitToPage(true);
		int rowCount = 0;
		Row header = sheetA.createRow(rowCount++);
		header.createCell(0).setCellValue("用户名");
		header.createCell(1).setCellValue("用户密码");
		header.createCell(2).setCellValue("备注");
		for (LoginInfo l : list) {
			Row currencyRow = sheetA.createRow(rowCount++);
			currencyRow.createCell(0).setCellValue(l.getUserName());
			currencyRow.createCell(1).setCellValue(l.getUserPass());
			currencyRow.createCell(2).setCellValue("");
		}
		//
		Sheet sheetB = workbook.createSheet("第二个sheet");
		sheetB.setFitToPage(true);
		response.setHeader("Content-Disposition", "attachment; filename=loginInfos.xls");

	}

}
