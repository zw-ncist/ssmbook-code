package learn.springwebmvc;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.format.FormatterRegistry;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.handler.HandlerExceptionResolverComposite;
import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;
import org.springframework.web.servlet.view.BeanNameViewResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.util.UrlPathHelper;

@Configuration
@EnableWebMvc
@ComponentScan("learn.springwebmvc")
public class MvcConfig implements WebMvcConfigurer {

	@Bean
	public ViewResolver beanNameViewResolver() {
		BeanNameViewResolver resolver = new BeanNameViewResolver();
		resolver.setOrder(-1);// 设置order值，小于0。这样其优先级高于ViewResolverComposite的0
		return resolver;
	}

	@Bean("excelView")
	public LoginInfosExcelView myExcelView() {
		LoginInfosExcelView view = new LoginInfosExcelView();
		return view;
	}

	@Override
	public void configureViewResolvers(ViewResolverRegistry registry) {
		registry.jsp("/WEB-INF/pages/", ".jsp");
		// 会创建一个InternalResourceViewResolver类型的解析器对象，这个对象又会被包含在ViewResolverComposite对象（order值为0）中

	}

	@Override
	public void configurePathMatch(PathMatchConfigurer configurer) {
		// 为了启用矩阵变量，在MVC Java配置中，需要将UrlPathHelper的removeSemicolonContent设置为false。
		UrlPathHelper urlPathHelper = new UrlPathHelper();
		urlPathHelper.setRemoveSemicolonContent(false);
		configurer.setUrlPathHelper(urlPathHelper);
	}

	@Bean // 兼容Servlet3.0的StandardServletMultipartResolver
	public MultipartResolver multipartResolver() {
		return new StandardServletMultipartResolver();
	}

	@Override
	public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {

		for (int i = 0; i < converters.size(); i++) {
			if (converters.get(i) instanceof StringHttpMessageConverter) {
				converters.set(i, new StringHttpMessageConverter(StandardCharsets.UTF_8));// 换掉它
				// 在使用@ResponseBody返回普通的字符串时，会使用这个Converter，需要设置一下编码方式。防止乱码
				System.out.println("===替换使用默认字符集iso-8859-1的StringHttpMessageConverter!");
			}
			System.out.println(
					converters.get(i).getClass().getSimpleName() + ">>" + converters.get(i).getSupportedMediaTypes());
		}
		converters.add(new LoginInfoConverter());// 加入自定义的Converter

	}

	@Override
	public void addFormatters(FormatterRegistry registry) {// 注册一个formater，全局的
		StudentFormatter sf = new StudentFormatter();
//			registry.addConverter(converter);
		registry.addFormatter(sf);// 加入自定义Formatter
	}

	// 定义SimpleMappingExceptionResolver的bean
	@Bean
	HandlerExceptionResolver errorHandler() {
		SimpleMappingExceptionResolver resolver = new SimpleMappingExceptionResolver();
		resolver.setExceptionAttribute("error");// 修改为模型属性“error”
		// 默认情况下，使用模型属性“exception”保存异常对象，。

		// 定义 异常到视图名的映射
		Properties ex2view = new Properties();
		ex2view.setProperty(InvalidCharacterException.class.getName(), "errorPage");

		resolver.setExceptionMappings(ex2view);

		// 定义 视图名到状态码的映射
		resolver.addStatusCode("errorPage", 400);// 400 Bad Request 客户端请求的语法错误，服务器无法理解

		// 默认的错误视图
		resolver.setDefaultErrorView("defaultErrorPage");
		// 默认的状态码
		resolver.setDefaultStatusCode(403);// 403 Forbidden 服务器理解请求客户端的请求，但是拒绝执行此请求
		// resolver没有设置order，所以采用默认值 即MAX_VALUE = 0x7fffffff，所以实际上排在默认的resovler的最后。
		return resolver;
	}

	@Bean // 定义HandlerExceptionResolverComposite的bean
	HandlerExceptionResolver exceptionResolverComposite() {
		// 创建2个自定义resolver
		HandlerExceptionResolverA resolverA = new HandlerExceptionResolverA();
		HandlerExceptionResolverB resolverB = new HandlerExceptionResolverB();
		// 创建一个HandlerExceptionResolverComposite实例，并加入2个resolver
		HandlerExceptionResolverComposite resolverComposite = new HandlerExceptionResolverComposite();
		resolverComposite.setExceptionResolvers(Arrays.asList(resolverA, resolverB));
		// HandlerExceptionResolverComposite中的解析器按照list的插入顺序进行迭代。

		resolverComposite.setOrder(-1);
		// 为resolverComposite设置顺序，数字越小，优先级越高;>0,则排在默认的resolver后，<0，则排在默认的resolver前。
		return resolverComposite;
	}

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(new MyInterceptor("A")).addPathPatterns("/interceptor/*");// 加入自定义拦截器，并且设置路径模式
		registry.addInterceptor(new MyInterceptor("B")).addPathPatterns("/interceptor/*");
	    LocaleChangeInterceptor localChange = new LocaleChangeInterceptor();
	    localChange.setParamName("customerlocale");//设置参数名
        registry.addInterceptor(localChange).addPathPatterns("/i18n/interceptor/readMessage");//拦截指定的处理方法
		
	}

	@Bean
	public MessageSource messageSource_Backup() {// 配置消息源 Bean的名称必须为messageSource
		ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
		messageSource.setBasenames("learn/springwebmvc/mymessage");// 给出资源文件的位置+basename
		return messageSource;
	}

	@Bean
	public MessageSource messageSource() {
		ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
		messageSource.setBasename("classpath:learn/springwebmvc/mymessage");
		messageSource.setDefaultEncoding("UTF-8");		
		messageSource.setCacheMillis(60000);//每隔60000毫秒更新一次
		return messageSource;
	}
	
//	  @Bean
//	    public LocaleResolver localeResolver(){
//	        SessionLocaleResolver r = new SessionLocaleResolver();
//	        r.setDefaultLocale(Locale.US);//设置默认的Locale， "en","US"
//	        return r;
//	    }
	    @Bean
	    public LocaleResolver localeResolver () {
	        CookieLocaleResolver r = new CookieLocaleResolver();
	        r.setDefaultLocale(Locale.US);
	        r.setCookieName("customerLocale");
	        r.setCookieMaxAge(-1);//当浏览器关闭时删除cookie
	        return r;
	    }

}
