package learn.springwebmvc;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class TestSimpleMappingExceptionResolver {
	@RequestMapping("/simpleMappingExceptionResolver") // http://localhost:8080/myhomework/mvc/simpleMappingExceptionResolver?name=a_b
    @ResponseBody
	public String test(String name) throws InvalidCharacterException {
    	if(name.contains("_")) {
    		
    		throw new InvalidCharacterException("�����Ƿ��ַ�: _");
    	}
		return name;
	}  
}
