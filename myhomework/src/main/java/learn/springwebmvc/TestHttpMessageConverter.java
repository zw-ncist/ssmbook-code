package learn.springwebmvc;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;

@Controller
@RequestMapping("httpMessageConverter")
public class TestHttpMessageConverter {
	
	
	@RequestMapping(value = "/form", method = RequestMethod.POST)
	@ResponseBody
	public String receiveForm(@RequestBody byte[] bodyByte, @RequestBody String bodyString,
			@RequestBody MultiValueMap<String, String> maps) {// 将请求体转换为byte[],String,MultiValueMap
		System.out.println("请求体(字节): " + bodyByte);
		System.out.println("请求体(字符串): " + bodyString);
		System.out.println("请求体(Map字符串): " + maps);	
		return "成功!";

	}

	@RequestMapping(value = "/xml", method = RequestMethod.POST, consumes = MediaType.APPLICATION_XML_VALUE)
	@ResponseBody
	public LoginInfo receiveXMLByAjax(@RequestBody LoginInfo loginInfo) {// 将xml文本转换为对象
		System.out.println(loginInfo);
		return loginInfo;
	}

	@RequestMapping(value = "/json", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public LoginInfo receiveJSONByAjax(@RequestBody LoginInfo loginInfo) {
		System.out.println(loginInfo);
		return loginInfo;
	}

	@RequestMapping(value = "/loginInfo", method = RequestMethod.POST, consumes = "text/logininfo")
	// 注意consumes和Ajax脚本中的contentType和dataType匹配
	@ResponseBody // 使用能够将对象LoginInfo转换为text/plain的HttpMessageConverter，所以会去找LoginInfoConverter
	public LoginInfo receiveLoginInfoByAjax(@RequestBody LoginInfo loginInfo) {
		System.out.println("转换后的对象=" + loginInfo);
		return loginInfo;
	}
}
