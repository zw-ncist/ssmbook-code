package learn.springwebmvc;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)//使用注解后，不会收到 “HTTP状态 500 - 内部服务器错误”
public class InvalidName extends Exception {
	public InvalidName(String message) {
		super(message);
	}
}
