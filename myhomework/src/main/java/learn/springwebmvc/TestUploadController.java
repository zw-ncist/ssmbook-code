package learn.springwebmvc;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSONObject;

@Controller
@RequestMapping("/upload")
public class TestUploadController {

	@RequestMapping(path = "/byRequestParam", method = RequestMethod.POST)// URL  http://localhost:8080/myhomework/uploadFile.jsp
	@ResponseBody
	public String upload1(@RequestParam("note") String note, LoginInfo loginInfo,
			@RequestParam Map<String, MultipartFile> files) throws IOException {//普通表单：含参数和文件提交到此处。 一次接受多文件
		// note是从请求参数获得，loginInfo的各个属性值也来自请求参数
		// files是一个Map类型的对象，存储了两个待上传的MultipartFile
		StringBuilder result = new StringBuilder();
		for (String key : files.keySet()) {
			String fileName = files.get(key).getOriginalFilename();// 文件名，Chrome，Sogou，EDGE浏览器获得的是不含路径的纯文件名，而IE会包含全部路径
			result.append(fileName + "+");
			System.out.println(fileName + "+");
			files.get(key).transferTo(new File("/" + fileName));// 保存到指定路径
		}
		return loginInfo + "=" + note + ":" + result.toString();
	}

	@RequestMapping(path = "/byRequestPart", method = RequestMethod.POST)		//Ajax：混合Json对象和文件提交到此处
	@ResponseBody
	public JSONObject upload2(@RequestParam("note") String note, @RequestPart("loginInfo") LoginInfo loginInfo,
			@RequestPart("fileOne") MultipartFile fileOne, @RequestParam MultipartFile fileTwo) throws IOException {

		// @RequestParam可以正常获取参数，@RequestPart会自动完成loginInfo的转换（从JSON串对象）,
		// 用@RequestPart也可以获取MultipartFile，用@RequestParam也可以。
		//@RequestParam更倾向于获取 name-value形式的输入作用域的值
		//@RequestPart更倾向于获取包含在multipart中复杂类型的数据例如json,xml等
		JSONObject resultJson = new JSONObject();//由于需要返回一个对象，这里使用阿里的JSONObject
		resultJson.put("note参数", note);
		resultJson.put("登录信息对象", loginInfo);
		resultJson.put("文件1对象", fileOne.getOriginalFilename());// 文件名，Chrome，Sogou，EDGE浏览器获得的是不含路径的纯文件名，而IE会包含全部路径
		resultJson.put("文件2对象", fileTwo.getOriginalFilename());

		fileOne.transferTo(new File("/" + fileOne.getOriginalFilename()));// 保存到指定路径
		fileTwo.transferTo(new File("/" + fileTwo.getOriginalFilename()));

		System.out.println(resultJson.toJSONString());
		return resultJson;

	}

}
