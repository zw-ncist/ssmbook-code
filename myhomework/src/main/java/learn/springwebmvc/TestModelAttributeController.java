package learn.springwebmvc;

import java.awt.Color;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/modelAttribute")
@SessionAttributes(names = {"info_session"}) // 只要符合的对象，一旦被加入到Model，则会被存至Session
public class TestModelAttributeController {
	@RequestMapping("/test")// http://localhost:8080/myhomework/mvc/modelAttribute/test
	public String test(Map<String, Object> map,Model model,ModelMap modelMap) {//验证ModelAndView与Model,Map,ModelMap三个形参的关系。
		if(map==model) {System.out.println("map==model");}
		if(map==modelMap) {System.out.println("map==modelMap");}
		if(modelMap==model) {System.out.println("modelMap==model");}
		//model,modelMap,map实际引用了同一个对象
		System.out.println("map is "+map.getClass().toString());
		System.out.println("model is "+model.getClass().toString());
		System.out.println("modelMap is "+modelMap.getClass().toString());
		
		map.put("mycolor", "map_color");
		model.addAttribute("mycolor", "model_color");
		modelMap.addAttribute("mycolor","modelMap_color");
		//所以这条语句会覆盖前面的重名属性
		
		//与ModelAndView中的模型不是一个对象
		ModelAndView mv=new ModelAndView();
		mv.addObject("mycolor4","MV_color");//不会被保存下来，因为这个控制器方法的返回值不是MV
		ModelMap mv_model_map=(ModelMap) mv.getModel();//实际调用 mv的getModelMap();
		ModelMap mv_modelMap=mv.getModelMap();//实际返回了 mv的 this.model属性 ，这是是一个ModelMap类型;
		System.out.println("mv_modelMap is"+mv_modelMap.getClass().toString());
		if(model==mv_modelMap) {
			System.out.println("model==mv_modelMap");
			
		}else {
			System.out.println("model!=mv_modelMap");
		}
		
		return "modelAttribute/read_model";
	}
	@RequestMapping("/testMV")//http://localhost:8080/myhomework/mvc/modelAttribute/testMV
	public ModelAndView testMV(Map<String, Object> map,Model model,ModelMap modelMap) {//验证ModelAndView与Model,Map,ModelMap三个形参的关系。
		//存储三个对象到模型中
		map.put("mycolor1", "map_color");
		model.addAttribute("mycolor2", "model_color");
		modelMap.addAttribute("mycolor3","modelMap_color");
		
		//与ModelAndView中的模型不是一个对象
		ModelAndView mv=new ModelAndView();
		mv.addObject("mycolor4", "mv_color");//会被保存下来
		mv.setViewName("modelAttribute/read_model");
	
		return mv;//如果不返回ModelAndView，则MV中加入的对象不会保存下来
	}
	
	
	@ModelAttribute("info2")
	public LoginInfo initB() {// 方法被@ModelAttribute标记，会被提前执行
		LoginInfo info = new LoginInfo();
		info.setUserName("Jerry");
		info.setUserPass("789123");
		System.out.println("initB");
		return info;// 返回值会被自动增加到Model
	}

	@ModelAttribute
	public void initA(Model m) {// 方法被@ModelAttribute标记，会被提前执行
		LoginInfo info1 = new LoginInfo();
		info1.setUserName("Tom");
		info1.setUserPass("123");
		System.out.println("initA");
		m.addAttribute(info1);//模型中的属性名由对象的类型转换而来，因此此处为“loginInfo”
		//如果不使用@ModelAttribute标记方法的话，仅仅使用Model.addAttribute方法，后面的@ModelAttribute("loginInfo") LoginInfo info1无法取出对象
	}

	@RequestMapping("read_attr") // http://localhost:8080/myhomework/mvc/modelAttribute/read_attr
	public String read_attr(@ModelAttribute("loginInfo") LoginInfo info1, @ModelAttribute("info2") LoginInfo info2,Model m) {// 形参列表加入Model对象
		String s = info1 + "," + info2;
		
		LoginInfo info_session = new LoginInfo();
		info_session.setUserName("info_session");
		info_session.setUserPass("info_session");
		m.addAttribute("info_session",info_session);//虽然没有使用@ModelAttribute注解，但是符合@SessionAttributes的条件。	
		System.out.println("read_attr=" + s);	
		return "forward:second_read_attr";// 做个转发
	}

	@RequestMapping("second_read_attr")//由于和@ModelAttribute配合使用，所以接下来会由 WEB-INF/pages/modelAttribute/second_read_attr.jsp 负责生成响应
	@ModelAttribute("info3")//会将返回值放置在模型属性中 "info3"=returnObject
	public LoginInfo second_read_attr(@ModelAttribute("info_session") LoginInfo info_session) {// 读取由Model和Session中存储的属性
		LoginInfo info = new LoginInfo();
		info.setUserName("Kevin");
		info.setUserPass("456");
		System.out.println("second_read_attr:"+info_session);//输出info_session对象
		return info;
	}	
	@RequestMapping("byPath/{userName}/{userPass}")// http://localhost:8080/myhomework/mvc/modelAttribute/byPath/Jackson/7658
	public String byPath(@ModelAttribute("info_byPath") LoginInfo info) {
		info.setUserName("info_byPath");//由于@ModelAttribute("info_byPath")并不存在，所以会创建和初始化（利用路径参数或请求参数），并自动存于Model
		info.setUserPass("info_byPath");
		return "modelAttribute/second_read_attr";//由 /WEB-INF/pages/modelAttribute/second_read_attr.jsp 负责生成响应
	}
	@RequestMapping("byParam")// http://localhost:8080/myhomework/mvc/modelAttribute/byParam?userName=Simth&userPass=15985
	public String byParam(@ModelAttribute("info_byParam") LoginInfo info) {
		info.setUserName("byParam");//由于@ModelAttribute("info_byPath")并不存在，所以会创建和初始化（利用路径参数或请求参数），并自动存于Model
		info.setUserPass("byParam");
		return "modelAttribute/second_read_attr";//由 /WEB-INF/pages/modelAttribute/second_read_attr.jsp 负责生成响应
	}
}
