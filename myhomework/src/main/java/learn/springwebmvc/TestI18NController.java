package learn.springwebmvc;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

@Controller
@RequestMapping("/i18n")
public class TestI18NController {
	@Autowired
	MessageSource messageSource;

	@RequestMapping("/interceptor/readMessage") // http://localhost:8080/myhomework/mvc/i18n/interceptor/readMessage?customerlocale=zh_CN
	@ResponseBody
	public String readMessage2(Locale locale) {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 日期格式
		String date = df.format(new Date());// 获取当前系统时间并格式化
		return messageSource.getMessage("index.welcome", new Object[] { "李四", locale.toString(), date }, locale);
	}
	
	
	@RequestMapping("/readMessage") // http://localhost:8080/myhomework/mvc/i18n/readMessage
	@ResponseBody
	public String readMessage(Locale locale) {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 日期格式
		String date = df.format(new Date());// 获取当前系统时间并格式化
		return messageSource.getMessage("index.welcome", new Object[] { "张三", locale.toString(), date }, locale);
	}

	@RequestMapping("/setSessionLocale") // http://localhost:8080/myhomework/mvc/i18n/setSessionLocale?localeId=1
	public String setSessionLocale(String localeId, Locale locale, HttpSession session) {// 获取locale

		Locale newlocale = null;
		if (localeId.equals("1")) {
			newlocale = new Locale("zh", "CN");
		} else {
			newlocale = new Locale("en", "US");
		}
		String str = "当前的Locale=" + locale+";修改后新的Locale="+newlocale;
		System.out.println(str);
		session.setAttribute(SessionLocaleResolver.LOCALE_SESSION_ATTRIBUTE_NAME, newlocale);
		return "forward:readMessage";
	}

	@RequestMapping("/showLocale") // http://localhost:8080/myhomework/mvc/i18n/showLocale
	@ResponseBody
	public Locale showLocale(Locale locale) {// 获取客户端的locale

		return locale;
	}

}
