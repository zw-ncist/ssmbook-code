package learn.spring.aop.service;

import java.sql.Timestamp;
import java.util.Date;

public class TimeStampComponentImpl implements TimeStampComponent {

	public String addTimeStamp(String originalStr) {

		Timestamp time = new Timestamp(new Date().getTime());

		return originalStr + time.toString();
	}

}
