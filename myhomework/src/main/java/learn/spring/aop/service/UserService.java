/**
 * 
 */
package learn.spring.aop.service;

import learn.spring.aop.MyLabel;

/**
 * @author StoneCenter
 *用户相关的业务逻辑
 */
@MyLabel
public interface UserService {
	public Boolean verify(String userID,String password) throws Exception;
}
