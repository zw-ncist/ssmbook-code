package learn.spring.aop.service;

public interface TimeStampComponent {
	public String addTimeStamp(String originalStr);
}
