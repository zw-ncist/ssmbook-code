package learn.spring.aop.service;

import learn.spring.aop.MyLabel;
import learn.spring.aop.MyMark;

public class UserServiceImpl implements UserService {
	@MyMark
	public Boolean verify(String userID, String password) throws Exception {
		if(userID.equals("admin")) {
			throw new Exception("不允许使用admin作为用户名！！！");
		}
		if(userID.equals("tom")&&password.equals("123")) {
			return true;
		}
		else {
			return false;
		}
	}

}
