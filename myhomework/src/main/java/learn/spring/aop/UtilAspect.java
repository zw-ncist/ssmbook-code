package learn.spring.aop;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.DeclareParents;

import learn.spring.aop.service.TimeStampComponent;
import learn.spring.aop.service.TimeStampComponentImpl;

@Aspect
public class UtilAspect {
	@DeclareParents(value="learn.spring.aop.service.UserService+", defaultImpl=TimeStampComponentImpl.class)
	public TimeStampComponent tsc;
}
