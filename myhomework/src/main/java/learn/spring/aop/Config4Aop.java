package learn.spring.aop;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

import learn.spring.aop.service.UserService;
import learn.spring.aop.service.UserServiceImpl;

@Configuration
@EnableAspectJAutoProxy
public class Config4Aop {
	@Bean
	public RecordAspect getRecordAspect() {
		RecordAspect ra = new RecordAspect();
		ra.setRecordFilePath("record.txt");// 会出现在 /myhomework/record.txt
		return ra;
	}
	@Bean("userService")
	public UserService getUserService() {
		UserService userService=new UserServiceImpl();
		//原对象是UserServiceImpl的实例，所以target()可以匹配UserServiceImpl和UserService
		//Spring默认使用JDK代理，会创建一个代理对象且实现接口UserService，所以this()可以匹配UserService，但不能匹配UserServiceImpl
		return userService;
	}
	@Bean("manager")
	public Manager getManager() {
		return new Manager();//
		//原对象是Manager的实例，所以target()可以匹配Manager
		//Spring此时使用Cglib代理，会创建一个代理对象，该对象的类型继承自Manager，所以this()可以匹配Manager
	}
	@Bean("subManager")
	public SubManager getSubManager() {
		return new SubManager();
	}
	@Bean("subSubManager")
	public SubSubManager getSubSubManager() {
		return new SubSubManager();
	}
	@Bean
	public UtilAspect getUtilAspect() {
		UtilAspect ua = new UtilAspect();	
		return ua;
	}
	@Bean
	public AdvancedRespect getAdvancedAspect() {
		return new AdvancedRespect();
	}

}
