package learn.spring.aop;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

@Aspect
public class AdvancedRespect {
	@Before("this(learn.spring.aop.service.UserService)")//代理对象的类型匹配到UserService时
	public void match_this() {
		System.out.println("match this(UserService)!");
	}
	@Before("target(learn.spring.aop.service.UserService)")//目标对象（原对象）的类型匹配到UserService时
	public void match_target() {
		System.out.println("match target(UserService)!");
	}
	
	@Before("this(learn.spring.aop.service.UserServiceImpl)")//代理对象的类型匹配到UserServiceImpl时
	public void match_this2() {
		System.out.println("match this(UserServiceImpl)!");
	}
	@Before("target(learn.spring.aop.service.UserServiceImpl)")//目标对象（原对象）的类型匹配到UserServiceImpl时
	public void match_target2() {
		System.out.println("match target(UserServiceImpl)!");
	}
	
	@Before("this(learn.spring.aop.Manager)")//代理对象的类型匹配到Manager时
	public void match_this3() {
		System.out.println("match this(Manager)!");
	}
	@Before("target(learn.spring.aop.Manager)")//目标对象（原对象）的类型匹配到Manager时
	public void match_target3() {
		System.out.println("match target(Manager)!");
	}
	@Before("@target(learn.spring.aop.MyLabel)")//目标对象（原对象）的类型使用了@MyLabel注解
	public void match_myLabel_annotation(){
		System.out.println("match @target(MyLabel)!");
	}
	
	@Before("@annotation(learn.spring.aop.MyMark)")//方法使用了@MyMark注解时匹配
	public void match_mymark_annotation(){
		System.out.println("match @annotation(MyMark)!");
	}
	@Before("@within(learn.spring.aop.MyLabel)")//目标对象（原对象）所属的类型使用了@MyLabel注解
	public void match_annotation_within() {
		System.out.println("match @within(MyLabel)!");
	}
	
	@Before("@args(learn.spring.aop.MyLabel)")//方法参数所属的类型使用了@MyLabel注解
	public void match_annotation_args() {
		System.out.println("match @args(MyLabel)!");
	}
}
