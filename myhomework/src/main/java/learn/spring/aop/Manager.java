package learn.spring.aop;
import learn.spring.aop.service.UserService;
@MyLabel
public class Manager {
	@MyMark
	public void beFired() {
	}
	@MyMark
	public void beEmployed() {
		
	}
	public void checkUserService(UserService us) {
	}
}
