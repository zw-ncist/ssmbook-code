package learn.spring.aop;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.ZoneId;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class RecordAspect {
	private String recordFilePath;

	public String getRecordFilePath() {
		return recordFilePath;
	}

	public void setRecordFilePath(String recordFilePath) {
		this.recordFilePath = recordFilePath;
	}

	@Pointcut("execution(Boolean learn.spring.aop.service.UserService.verify(..))") // 定义一个切点
	public void verify_pointcut() {
	}

	@Before("within(learn.spring.aop.service.*)") // 匹配learn.spring.aop.service包中的所有类的所有方法
	public void recordBeforeTime() {
		LocalDateTime localDateTime = LocalDateTime.now(ZoneId.of("Asia/Shanghai"));
		String record = "@Before 时间=" + localDateTime.toString();
		writeRecord(record);
	}

	@Before("execution(* learn.spring.aop.service.UserService.verify(String,String))&&args(userID,password)") // learn.spring.aop.service.UserService类中的verify方法，且该方法具有两个String类型的参数。同时利用args(userID,password)向advice传递两个名为userID,password的参数
	public void recordVerifyInfo(String userID, String password) {
		String record = "@Before VerifyInfo=" + userID + ",password=" + password;
		writeRecord(record);
	}

	@AfterReturning(pointcut = "execution(Boolean learn.spring.aop.service.UserService.verify(..))", returning = "result") // 正常返回时，使用执行结果
	public void recordVerifyResult(Boolean result) {
		String record = "@AfterReturning VerifyResult=" + result;
		writeRecord(record);
	}

	@AfterThrowing(pointcut = "verify_pointcut()", throwing = "exception")
//	@AfterThrowing(pointcut="execution(public * learn.spring.aop.service.UserService.verify(..))",throwing="exception")
	public void recordVerrifyException(Exception exception) {
		String record = "@AfterThrowing VerifyException=" + exception;
		writeRecord(record);
	}

	@After("execution(* learn.spring.aop.service.*.*(..))") // service包下，任意类的任意方法
	public void recordAfterTime() {
		LocalDateTime localDateTime = LocalDateTime.now(ZoneId.of("Asia/Shanghai"));
		String record = "@After 时间=" + localDateTime.toString();
		writeRecord(record);
	}

	@Around("execution(** learn.spring.aop.service.UserService.verify(String,String))")//verify方法，该方法的访问修饰符任意，而且可以有任意类型的返回值，但必须具有两个String类型的参数。
	public Object recordAround(ProceedingJoinPoint pjp) throws Throwable {
		Object re = null;
		writeRecord("@Around...在proceed之前");
		Object[] args = pjp.getArgs();
		Signature signature = pjp.getSignature();
		writeRecord("@Around...args=" + getStr(args) + ",signature=" + signature);

		if (args[0].equals("") || args[1].equals("")) {// 可以不调用
			writeRecord("@Around...用户密码为空串,不会调用verify！");
		} else {
			writeRecord("@Around...调用verify！");
			re = pjp.proceed();
		}

		writeRecord("@Around...在proceed之后");
		return re;
	}

	private static String getStr(Object[] obs) {
		String s = "";
		for (Object o : obs) {
			s += o.toString() + "\t";
		}
		return s;
	}

	private void writeRecord(String record) {
		FileWriter fileWriter = null;
		try {
			File f = new File(recordFilePath);
			fileWriter = new FileWriter(f, true);// append
		} catch (IOException e) {
			e.printStackTrace();
		}
		PrintWriter printerWriter = new PrintWriter(fileWriter);
		printerWriter.println(record);
		printerWriter.flush();
		try {
			fileWriter.flush();
			printerWriter.close();
			fileWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
