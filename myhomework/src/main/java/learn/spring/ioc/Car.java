package learn.spring.ioc;

public class Car {
	private String modelName;
	private Motor motor;
	public String getModelName() {
		return modelName;
	}
	public void setModelName(String modelName) {
//		System.out.println("注入Car.modelName");
		this.modelName = modelName;
	}
	public Motor getMotor() {
		return motor;
	}
	public void setMotor(Motor motor) {
//		System.out.println("注入Car.motor");
		this.motor = motor;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return modelName+"采用的发动机->"+motor.toString();
	}
	
}
