package learn.spring.ioc;

public class Motor {
	private float displacement;//����
	private int cylinders;//������
	
	public Motor() {
		super();
	}
	public Motor(float displacement, int cylinders) {
		super();
		this.displacement = displacement;
		this.cylinders = cylinders;
	}
	public float getDisplacement() {
		return displacement;
	}
	public void setDisplacement(float displacement) {
		this.displacement = displacement;
	}
	public int getCylinders() {
		return cylinders;
	}
	public void setCylinders(int cylinders) {
		this.cylinders = cylinders;
	}
	@Override
	public String toString() {
		return "Motor [displacement=" + displacement + ", cylinders=" + cylinders + "]";
	}
	
}
