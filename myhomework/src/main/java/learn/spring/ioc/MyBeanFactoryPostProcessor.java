package learn.spring.ioc;

import org.springframework.beans.BeansException;
import org.springframework.beans.PropertyValue;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;

public class MyBeanFactoryPostProcessor implements BeanFactoryPostProcessor {

	public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
		// TODO Auto-generated method stub
		System.out.println("工厂后执行"+"BeanFactoryPostProcessor.postProcessBeanFactory()方法");
		BeanDefinition bd=beanFactory.getBeanDefinition("car");//获得配置文件中指定对象的定义信息
		for(PropertyValue pv:bd.getPropertyValues().getPropertyValues()) {
			System.out.println(pv.getName()+"="+pv.getValue());
		}
	}

}
