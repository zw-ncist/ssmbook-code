package learn.spring.ioc;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class Wheel implements BeanNameAware,BeanFactoryAware,ApplicationContextAware,InitializingBean,DisposableBean {
	private float size;
	
	public Wheel() {
		super();
		System.out.println("调用构造函数");
	}

	public float getSize() {
		return size;
	}

	public void setSize(float size) {
		System.out.println("注入Wheel.size");
		this.size = size;
	}

	public void destroy() throws Exception {
		// TODO Auto-generated method stub
		System.out.println("执行"+"DisposableBean.destroy()");
		
	}

	public void afterPropertiesSet() throws Exception {
		// TODO Auto-generated method stub
		System.out.println("执行"+"InitializingBean.afterPropertiesSet()");
	}

	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		// TODO Auto-generated method stub
		System.out.println("执行"+"ApplicationContextAware.setApplicationContext(),获得ApplicationContext对象"+applicationContext.toString());
	}

	public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
		// TODO Auto-generated method stub
		System.out.println("执行"+"BeanFactoryAware.setBeanFactory(),获得BeanFactory对象"+beanFactory.toString());
	}

	public void setBeanName(String name) {
		// TODO Auto-generated method stub
		System.out.println("执行"+"BeanNameAware.setBeanName(),获得bean的name="+name);
	}

	public void my_init() {
		System.out.println("执行"+"自定义初始化对象的方法my_init");
	}
	public void my_destroy() {
		System.out.println("执行"+"自定义销毁对象的方法my_destroy");
	}
}
