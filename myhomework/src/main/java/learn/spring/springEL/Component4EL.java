package learn.spring.springEL;

import java.awt.Color;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Component4EL {
	@Value("#{true}")//逻辑值
	Boolean t;
	@Value("#{1.2+2.8}")//运算结果
	Float f;
	@Value("#{'I am a String!'}")//字符串
	String s;
	@Value("#{mycolor}")//注入一个bean
	Color color;
	@Value("#{mycolor.getRed()}")//bean方法调用的结果
	int color_red;
	@Value("#{mylamp.color}")//bean的属性
	Color lamp_color;
	@Value("#{mylamp?.color}")//安全情况下使用bean
	Color lamp2_color;
	@Value("#{T(java.awt.Color).red}")//类的静态字段
	Color static_color;
	@Value("#{T(java.awt.Color).getColor('blue')}")//类的静态方法
	Color static_method_color;
	@Value("#{namelist[1].length()}")//使用集合或数组中的元素
	int namelist_name_length;
	public Boolean getT() {
		return t;
	}

	public void setT(Boolean t) {
		this.t = t;
	}

	public Float getF() {
		return f;
	}

	public void setF(Float f) {
		this.f = f;
	}

	public String getS() {
		return s;
	}

	public void setS(String s) {
		this.s = s;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public int getColor_red() {
		return color_red;
	}

	public void setColor_red(int color_red) {
		this.color_red = color_red;
	}

	public Color getLamp_color() {
		return lamp_color;
	}

	public void setLamp_color(Color lamp_color) {
		this.lamp_color = lamp_color;
	}

	public Color getLamp2_color() {
		return lamp2_color;
	}

	public void setLamp2_color(Color lamp2_color) {
		this.lamp2_color = lamp2_color;
	}

	public Color getStatic_color() {
		return static_color;
	}

	public void setStatic_color(Color static_color) {
		this.static_color = static_color;
	}

	public Color getStatic_method_color() {
		return static_method_color;
	}

	public void setStatic_method_color(Color static_method_color) {
		this.static_method_color = static_method_color;
	}

	public int getNamelist_name_length() {
		return namelist_name_length;
	}

	public void setNamelist_name_length(int namelist_name_length) {
		this.namelist_name_length = namelist_name_length;
	}

	@Override
	public String toString() {
		return "Component4EL [t=" + t + ", f=" + f + ", s=" + s + ", color=" + color + ", color_red=" + color_red
				+ ", lamp_color=" + lamp_color + ", lamp2_color=" + lamp2_color + ", static_color=" + static_color
				+ ", static_method_color=" + static_method_color + ", namelist_name_length=" + namelist_name_length
				+ "]";
	}
	
	
	
}
