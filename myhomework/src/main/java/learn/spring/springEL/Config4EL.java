package learn.spring.springEL;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan
public class Config4EL {

	@Bean("mycolor")
	public Color getMyColor() {
		return Color.lightGray;
	}

	@Bean("mylamp")
	public Lamp getMyLamp() {		
		Lamp lamp=new Lamp();
		lamp.color=Color.yellow;
		return  lamp;
	}
	
	@Bean("namelist")
	public List<String> getColorList(){
		List<String> l=new ArrayList<String>();
		l.add("Vivian");
		l.add("David");
		l.add("Kevin");
		return l;
	}

}
