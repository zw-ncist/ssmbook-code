package learn.spring.springMybatis;

import java.io.InputStream;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.tomcat.dbcp.dbcp2.BasicDataSourceFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;

@Configuration
@MapperScan("persistence.mybatis.mapper")//自动扫描Mapper接口
public class Config4SpringMybatis {
	@Bean("dataSource")
	public DataSource dbcpDataSource() {
		 DataSource dataSource=null;
		try {
			//从properties创建Data Source
			InputStream inputStream =  Config4SpringMybatis.class.getClassLoader().getResourceAsStream("learn/spring/springMybatis/dbcp.properties");
			Properties props = new Properties();
			props.load(inputStream);
			dataSource= BasicDataSourceFactory.createDataSource(props);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return dataSource;
	}

	@Bean("sqlSessionFactory")
	public SqlSessionFactory sqlSessionFactory() throws Exception {
		ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
		
		SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
		
		sqlSessionFactoryBean.setDataSource(dbcpDataSource() );//注入DataSource
//		sqlSessionFactoryBean.setConfigLocation(new ClassPathResource("learn/spring/springMybatis/spring-mybatis-config.xml"));这个功能一样
		sqlSessionFactoryBean.setConfigLocation(resolver.getResource("classpath:learn/spring/springMybatis/spring-mybatis-config.xml"));//MyBatis的基础配置文件
		Resource mappers[]=resolver.getResources("classpath:persistence/mybatis/mapper/*.xml");//mapper的xml文件，可以使用通配符
		for(int i=0;i<mappers.length;i++) {
			System.out.println(mappers[i]);
		}

		sqlSessionFactoryBean.setMapperLocations(mappers);
		return sqlSessionFactoryBean.getObject();
	}
}
