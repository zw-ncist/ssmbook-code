package learn.spring.assemble;

import java.awt.Color;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
@Component(value="chain")
public class Chain {
	@Value("16.5")
	private float length;
	@Autowired
	@Qualifier("mygreen")
	private Color color;
	public float getLength() {
		return length;
	}

	public void setLength(float length) {
		this.length = length;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	@Override
	public String toString() {
		return "Chain [length=" + length + ", color=" + color + "]";
	}
	
}
