package learn.spring.assemble;

import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
@Component("bicycle")
public class Bicycle {
	private String modelType;//型号
	private Chain chain;//链条
	private Frame frame;//车架
	private List<Pedal> pedals;//脚踏
	private Map<String,Float> retailPrice;//不同地区的零售价
	private Set<String> factories;//可以制造该车的工厂
	private Properties otherProps;//其它属性
	private String[] designers;//设计者
	public Bicycle() {	}
	public Bicycle(String modelType, Chain chain, Frame frame,List<Pedal> pedals) {
		this.modelType = modelType;
		this.chain = chain;
		this.frame = frame;
		this.pedals = pedals;
	}
	@Autowired
	public Bicycle(@Value("希望之路2") String modelType, Chain chain, Frame frame, List<Pedal> pedals, Map<String, Float> retailPrice,
			Set<String> factories, @Qualifier("otherProps") Properties otherProps,@Value("{'张三','李四'}") String[] designers) {
		super();
		this.modelType = modelType;
		this.chain = chain;
		this.frame = frame;
		this.pedals = pedals;
		this.retailPrice = retailPrice;
		this.factories = factories;
		this.otherProps = otherProps;
		this.designers = designers;
	}
	public String getModelType() {
		return modelType;
	}
	public void setModelType(String modelType) {
		this.modelType = modelType;
	}
	public Chain getChain() {
		return chain;
	}
	public void setChain(Chain chain) {
		this.chain = chain;
	}
	public Frame getFrame() {
		return frame;
	}
	public void setFrame(Frame frame) {
		this.frame = frame;
	}
	public List<Pedal> getPedals() {
		return pedals;
	}
	public void setPedals(List<Pedal> pedals) {
		this.pedals = pedals;
	}
	public Map<String, Float> getRetailPrice() {
		return retailPrice;
	}
	public void setRetailPrice(Map<String, Float> retailPrice) {
		this.retailPrice = retailPrice;
	}
	public Set<String> getFactories() {
		return factories;
	}
	public void setFactories(Set<String> factories) {
		this.factories = factories;
	}
	
	public String[] getDesigners() {
		return designers;
	}
	public void setDesigners(String[] designers) {
		this.designers = designers;
	}
	public String strDesigners() {
		String s="";
		for(int i=0;i<this.designers.length;i++) {
			s+=this.designers[i]+",";
		}
		return s;
		
	}
	
	
	public Properties getOtherProps() {
		return otherProps;
	}
	public void setOtherProps(Properties otherProps) {
		this.otherProps = otherProps;
	}
	@Override
	public String toString() {
		return "Bicycle [modelType=" + modelType + "\n chain=" + chain + "\n frame=" + frame + "\n pedals=" + pedals
				+ "\n retailPrice=" + retailPrice + "\n designers=" + this.strDesigners()+ "\n factories=" + factories +"\n properties=" + this.otherProps +  "]";
	}

	

}
