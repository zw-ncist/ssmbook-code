package learn.spring.assemble;

import java.awt.Color;

import org.springframework.context.annotation.Bean;

public class Frame {

	private String material;

	private Float retailPrice;

	private double length;

	private Color color;
	
	public Frame(String material, Float retailPrice, double length, Color color) {
		super();
		this.material = material;
		this.retailPrice = retailPrice;
		this.length = length;
		this.color = color;
	}
	public String getMaterial() {
		return material;
	}
	public void setMaterial(String material) {
		this.material = material;
	}
	public Float getRetailPrice() {
		return retailPrice;
	}
	public void setRetailPrice(Float retailPrice) {
		this.retailPrice = retailPrice;
	}
	public double getLength() {
		return length;
	}
	public void setLength(double length) {
		this.length = length;
	}
	public Color getColor() {
		return color;
	}
	public void setColor(Color color) {
		this.color = color;
	}
	@Override
	public String toString() {
		return "Frame [material=" + material + ", retailPrice=" + retailPrice + ", length=" + length + ", color="
				+ color + "]";
	}


}
