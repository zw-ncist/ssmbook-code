package learn.spring.assemble;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages ="learn.spring.assemble")
public class Config4Bicycle {
	@Bean(name="mygreen")
	public Color getMyGreen() {
		return Color.green;
	}
	
	@Bean(name="blueColor")
	public Color aBlueColor(){
		return new Color(0.0f,0.0f,1.0f);
	}
	
	@Bean
	public Frame aFrame(@Qualifier("blueColor") Color color){
		return new Frame("铝合金", 1288.5f, 1.586, color);
	}
	
	@Bean
	public List<Pedal> pedals(Pedal commonPedal) {
		List<Pedal> pedals=new ArrayList<Pedal>();
		pedals.add(commonPedal);
		Pedal p=new Pedal();
		p.setInfo("右侧脚踏");
		pedals.add(p);
		return pedals;
	}
	
	@Bean
	public Map<String,Float> retailPrice(){
		Map<String,Float> map=new HashMap<String,Float>();
		map.put("北京", 888f);
		map.put("上海", 999f);
		map.put("广州", 966f);
		return map;
	}
	
	@Bean
	public Set<String> factories(){
		Set<String> set=new HashSet<>();
		set.add("北京通州制造厂");
		set.add("天津武清制造厂");
		return set;
	}
	
	@Bean("otherProps")
	public Properties props() {
		Properties props=new Properties();
		props.setProperty("designVersion", "2.0.3");
		props.setProperty("shortComing", "前叉回弹不足");
		return props;
	}
}
