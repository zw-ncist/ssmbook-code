package learn.spring.transaction;

import java.time.LocalDateTime;
import java.time.ZoneId;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.core.annotation.Order;
@Aspect
public class TimeAspect {
	@Before("this(business.ManageCollegeInfo)") // 代理对象匹配business.ManageCollegeInfo接口
	public void recordBeforeTime() {
		LocalDateTime localDateTime = LocalDateTime.now(ZoneId.of("Asia/Shanghai"));
		String record = "@Before 时间=" + localDateTime.toString();
		System.out.println(record);
	}
}
