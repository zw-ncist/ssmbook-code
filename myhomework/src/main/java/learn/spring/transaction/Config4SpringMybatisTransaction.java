package learn.spring.transaction;

import java.io.InputStream;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.tomcat.dbcp.dbcp2.BasicDataSourceFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.TransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import learn.spring.aop.RecordAspect;

@Configuration
@EnableTransactionManagement //启用Spring的注释驱动的事务管理功能
@MapperScan("persistence.mybatis.mapper")//自动扫描Mapper接口
@ComponentScan("business")//自动扫描Service组件
@EnableAspectJAutoProxy
public class Config4SpringMybatisTransaction {
	@Bean("dataSource")
	public DataSource dbcpDataSource() {
		 DataSource dataSource=null;
		try {
			//从properties创建Data Source
			InputStream inputStream =  Config4SpringMybatisTransaction.class.getClassLoader().getResourceAsStream("learn/spring/transaction/dbcp.properties");
			Properties props = new Properties();
			props.load(inputStream);
			dataSource= BasicDataSourceFactory.createDataSource(props);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dataSource;
	}

	@Bean("sqlSessionFactory")
	public SqlSessionFactory sqlSessionFactory() throws Exception {
		ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
		SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
		sqlSessionFactoryBean.setDataSource(dbcpDataSource() );//注入DataSource
//		sqlSessionFactoryBean.setConfigLocation(new ClassPathResource("learn/spring/springMybatis/spring-mybatis-config.xml"));这个功能一样
		sqlSessionFactoryBean.setConfigLocation(resolver.getResource("classpath:learn/spring/transaction/spring-mybatis-config.xml"));//MyBatis的基础配置文件
		Resource mappers[]=resolver.getResources("classpath:persistence/mybatis/mapper/*.xml");//mapper的xml文件，可以使用通配符
		sqlSessionFactoryBean.setMapperLocations(mappers);
		return sqlSessionFactoryBean.getObject();
	}
	@Bean("tManager")
	public TransactionManager dataSourceTransactionManager() {
		DataSourceTransactionManager dstm=new DataSourceTransactionManager(dbcpDataSource());
		return dstm;
	}
	@Bean
	public TimeAspect getTimeAspect() {
		TimeAspect ta = new TimeAspect();
		return ta;
	}
}
