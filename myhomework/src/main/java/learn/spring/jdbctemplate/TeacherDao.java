package learn.spring.jdbctemplate;

import java.util.List;

import org.springframework.stereotype.Repository;

import persistence.mybatis.model.Teacher;

public interface TeacherDao {
	public int insertTeacher(Teacher newTeacher);
	public int deleteTeacher(Long teacherId);
	public int updateTeacher(Teacher modifiedTeacher);
	public List<Teacher> queryTeachersByCollege(Long collegeId);
	public Teacher findOneTeacherById(Long teacherId);
	public int countTeacher();
}
