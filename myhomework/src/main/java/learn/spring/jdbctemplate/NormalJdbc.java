package learn.spring.jdbctemplate;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class NormalJdbc {
	public static void main(String[] args) {
		Connection conn = null;
		Statement statement = null;
		try {
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/myhomework", "root", "111111");
			statement = conn.createStatement();
			ResultSet rs = statement.executeQuery("select count(*) from teacher ");
			if (rs.next()) {
				System.out.println("查询得到记录数=" + rs.getInt(1));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
