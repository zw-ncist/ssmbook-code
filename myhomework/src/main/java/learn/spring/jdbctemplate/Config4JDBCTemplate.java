package learn.spring.jdbctemplate;

import java.io.InputStream;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.tomcat.dbcp.dbcp2.BasicDataSourceFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

@Configuration
@ComponentScan(basePackages ="learn.spring.jdbctemplate")
public class Config4JDBCTemplate {
	@Bean("dataSource")
	public DataSource dbcpDataSource() {
		 DataSource dataSource=null;
		try {
			//从properties创建Data Source
			InputStream inputStream =  Config4JDBCTemplate.class.getClassLoader().getResourceAsStream("learn/spring/springMybatis/dbcp.properties");
			Properties props = new Properties();
			props.load(inputStream);
			dataSource= BasicDataSourceFactory.createDataSource(props);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return dataSource;
	}

	@Bean("jdbcTemplate")
	public JdbcTemplate getJdbctemplate() {
		
//		 每次运行SQL时都基本不需要创建JdbcTemplate类的新实例。
//		  JdbcTemplate实例就是线程安全的。
//		  如果应用程序访问多个数据库，那么可能需要多个JdbcTemplate实例，这需要多个数据源，并且随后需要多个不同配置的JdbcTemplate实例。

		return  new JdbcTemplate(dbcpDataSource());
		
	}
}
