package learn.spring.jdbctemplate;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import persistence.mybatis.model.Teacher;

@Repository("teacherDao")
public class TeacherDaoJdbc implements TeacherDao {
	@Autowired
//	@Qualifier("jdbcTemplate")//可以不写,容器中只有一个实例
	private JdbcTemplate jdbcTemplate;

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	/*
	 * 定义RowMapper用于向对象的转换，这里使用了Lambda表达式
	 * */
	private final RowMapper<Teacher> actorRowMapper = (resultSet, rowNum) -> {
		Teacher teacher = new Teacher();	    
		teacher.setTeacherId(resultSet.getLong("teacher_id"));
	    teacher.setCollegeId(resultSet.getLong("college_id"));
	    teacher.setTeacherName(resultSet.getString("teacher_name"));
	    return teacher;
	};
	
	@Override
	public int insertTeacher(Teacher newTeacher) {
		//如果需要获取自增的主键，则需要一些额外的处理
		KeyHolder keyHolder = new GeneratedKeyHolder();
		PreparedStatementCreator preparedStatementCreator = con -> {
			PreparedStatement ps = con.prepareStatement(
					"insert into teacher(teacher_name,college_id) values(?,?)",
					Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, newTeacher.getTeacherName());
			ps.setLong(2, newTeacher.getCollegeId());
			return ps;
		};
		
		int re=jdbcTemplate.update(preparedStatementCreator, keyHolder);
		newTeacher.setTeacherId(keyHolder.getKey().longValue());//读取自增的主键值
		return re;
		
		//如果不需要获取自增的主键值，则很简单
//		return this.jdbcTemplate.update("insert into teacher(teacher_name,college_id) values(?,?);",
//				newTeacher.getTeacherName(), newTeacher.getTeacherId());
	}

	@Override
	public int deleteTeacher(Long teacherId) {
		return this.jdbcTemplate.update("delete from teacher where teacher_id=?",
				teacherId);
	}

	@Override
	public int updateTeacher(Teacher modifiedTeacher) {
		return this.jdbcTemplate.update("update teacher set teacher_name=?,college_id=? where teacher_id=?",
				modifiedTeacher.getTeacherName(),modifiedTeacher.getCollegeId(),modifiedTeacher.getTeacherId());

	}

	@Override
	public List<Teacher> queryTeachersByCollege(Long collegeId) {
		return this.jdbcTemplate.query("select * from teacher where college_id=?", actorRowMapper,collegeId);//返回一个List<Teacher>
	}

	@Override
	public Teacher findOneTeacherById(Long teacherId) {
		return this.jdbcTemplate.queryForObject("select * from teacher where teacher_id=?", actorRowMapper,teacherId);//返回一个Teacher对象
	}

	@Override
	public int countTeacher() {
		return this.jdbcTemplate.queryForObject("select count(*) from teacher", Integer.class);//返回一个数
	}

}
