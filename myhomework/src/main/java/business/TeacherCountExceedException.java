package business;

public class TeacherCountExceedException extends RuntimeException {

	public TeacherCountExceedException(String message) {
		super("教师数已达最大值:"+message+"!");
		
	}
	
}
