package business;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import persistence.mybatis.mapper.CollegeInfoMapper;
@Service
public class ManageCollegeInfoImpl implements ManageCollegeInfo {
	@Autowired
	CollegeInfoMapper collegeInfoMapper;

	public CollegeInfoMapper getCollegeInfoMapper() {
		return collegeInfoMapper;
	}

	public void setCollegeInfoMapper(CollegeInfoMapper collegeInfoMapper) {
		this.collegeInfoMapper = collegeInfoMapper;
	}
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.READ_COMMITTED)
	public void modifyTeacherCount(Boolean increase, Long collegeId) {
		Integer oldCount = collegeInfoMapper.queryByCollegeId(collegeId).getTeacherCount();
		System.out.println("&&&&&&&&&&&更新前="+oldCount);
//		if (oldCount >= 8) {	//在更新前进行检查，会在多线程中引发问题
//		throw new TeacherCountExceedException("最大人数8");
//		}	
		if(increase==true) {					
			collegeInfoMapper.increaseTeacherCount(collegeId);
		}else {
			collegeInfoMapper.decreaseTeacherCount(collegeId);
		}
		Integer newCount = collegeInfoMapper.queryByCollegeId(collegeId).getTeacherCount();// 查询一下新的的教师数,防止并发事务时发生问题
		System.out.println("@@@@@@@@@@@更新后="+newCount);
		if (newCount > 8) {	
			throw new TeacherCountExceedException("最大人数8");
		}	
	}

}
