package business;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import persistence.mybatis.mapper.CollegeInfoMapper;
import persistence.mybatis.mapper.CollegeMapper;
import persistence.mybatis.mapper.TeacherMapper;
import persistence.mybatis.model.Teacher;

@Service
public class ManageTeacherImpl implements ManageTeacher {

	@Autowired
	TeacherMapper teacherMapper;

	public TeacherMapper getTeacherMapper() {
		return teacherMapper;
	}

	public void setTeacherMapper(TeacherMapper teacherMapper) {
		this.teacherMapper = teacherMapper;
	}

	@Autowired
	ManageCollegeInfo manageCollegeInfo;

	public ManageCollegeInfo getManageCollegeInfo() {
		return manageCollegeInfo;
	}

	public void setManageCollegeInfo(ManageCollegeInfo manageCollegeInfo) {
		this.manageCollegeInfo = manageCollegeInfo;
	}

	@Override
	public Long createNewTeacher(Long collegeId, String teacherName) {
		Long newTeacherId = null;
		Teacher t = new Teacher();// 插入一条Teacher记录
		t.setCollegeId(collegeId);
		t.setTeacherName(teacherName);
		teacherMapper.insertTeacher(t);
		newTeacherId = t.getTeacherId();
		manageCollegeInfo.modifyTeacherCount(true, collegeId);
		return newTeacherId;// 返回新的教师ID
	}

//	@Override
//	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED)
//	public void changeCollegeOfTeacher(Long newCollegeId, Long teacherId) {
//		Boolean result = false;
//		Teacher t = teacherMapper.findOneTeacherById(teacherId);
//		manageCollegeInfo.modifyTeacherCount(false, t.getCollegeId());
//		t.setCollegeId(newCollegeId);
//		teacherMapper.updateTeacher(t);
//		manageCollegeInfo.modifyTeacherCount(true, t.getCollegeId());
//
//	}

}
