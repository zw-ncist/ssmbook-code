package business;

public class TeacherNameForbiddenException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4822894298686415719L;

	public TeacherNameForbiddenException(String arg0) {
		super("教师名称"+arg0+"属于系统禁用范围!");
		
	}
	
}
