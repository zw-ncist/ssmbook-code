package business;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import persistence.mybatis.mapper.CourseMapper;
import persistence.mybatis.model.Course;
import persistence.mybatis.model.MyBatisUtils;

public class ManageCourseImpl implements ManageCourse {
	CourseMapper mapper=null;
	SqlSession sqlSession=null;
	public ManageCourseImpl() {
		this.sqlSession = MyBatisUtils.getSession();
		this.mapper = (CourseMapper) sqlSession.getMapper(CourseMapper.class);
	}

	public List<Course> getAllCourse() {
		// TODO Auto-generated method stub
		List<Course> result=this.mapper.getAll();
		this.sqlSession.close();
		return result;
	}

	public List<Course> queryCourseById(Long id) {
		List<Course> result=this.mapper.getCourseById(id);
		this.sqlSession.close();
		return result;
	}

	public boolean addNewCourse(Course c) {
		
		this.mapper.addCourse(c);
		this.sqlSession.close();
		return true;
	}

	public boolean delCourse(Long id) {
		this.mapper.delCourse(id);
		this.sqlSession.close();
		return true;
	}

	public boolean modifyCourse(Course modifiedCourse) {
		//这里增加一个单独的courseId的参数，主要是为了说明混合传参的过程
		this.mapper.updCourse(modifiedCourse.getCourseId(), modifiedCourse);
		this.sqlSession.close();
		return true;
	}

	public Integer queryStudentsCountRelatedToCourse(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

}
