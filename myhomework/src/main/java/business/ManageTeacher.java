package business;

import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED)
public interface ManageTeacher {
	public Long createNewTeacher(Long collegeId,String teacherName);//向指定学院增加一名教师
}
