package business;

import java.util.List;

import persistence.mybatis.model.Course;

/**
 * @author StoneCenter
 * 课程管理的业务逻辑
 */
public interface ManageCourse {
	public List<Course> getAllCourse();//获得所有课程信息
	public List<Course> queryCourseById(Long id);//查询指定id的课程
	public boolean addNewCourse(Course c);//增加一门新课
	public boolean delCourse(Long id);//删除一门课程
	public boolean modifyCourse(Course modifiedCourse);//修改课程信息
	public Integer queryStudentsCountRelatedToCourse(Long id);//查询与某一个课程相关的学生
}
