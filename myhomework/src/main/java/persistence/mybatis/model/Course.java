package persistence.mybatis.model;

public class Course {
	private Long courseId;
	private String courseName;
	private Double courseScore;
	public Long getCourseId() {
		return courseId;
	}
	public void setCourseId(Long id) {
		this.courseId = id;
	}
	public String getCourseName() {
		return courseName;
	}
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
	public Double getCourseScore() {
		return courseScore;
	}
	public void setCourseScore(double d) {
		this.courseScore = d;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return courseId+"-"+courseName+"-"+courseScore;
	}
	
}
