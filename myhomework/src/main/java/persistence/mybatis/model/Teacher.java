package persistence.mybatis.model;

import java.io.Serializable;

public class Teacher implements Serializable {

	private static final long serialVersionUID = 6900838355981735032L;
	private Long teacherId;
	private String teacherName;
	private Long collegeId;
	
	public Long getTeacherId() {
		return teacherId;
	}
	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}
	public String getTeacherName() {
		return teacherName;
	}
	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}
	public Long getCollegeId() {
		return collegeId;
	}
	public void setCollegeId(Long collegeId) {
		this.collegeId = collegeId;
	}
	@Override
	public String toString() {
		return "Teacher [teacherId=" + teacherId + ", teacherName=" + teacherName + ", collegeId=" + collegeId + "]";
	}
	
	
}
