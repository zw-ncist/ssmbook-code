package persistence.mybatis.model;

import java.io.Serializable;

public class Major implements Serializable{

	private static final long serialVersionUID = 1142670790446150234L;

	private Long majorId;

    private String majorName;

    private Long collegeId;

    public Long getMajorId() {
        return majorId;
    }

    public void setMajorId(Long majorId) {
        this.majorId = majorId;
    }

    public String getMajorName() {
        return majorName;
    }

    public void setMajorName(String majorName) {
        this.majorName = majorName;
    }

    public Long getCollegeId() {
        return collegeId;
    }

    public void setCollegeId(Long collegeId) {
        this.collegeId = collegeId;
    }
}