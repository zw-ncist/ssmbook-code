package persistence.mybatis.model;

import java.io.Serializable;
import java.util.List;

public class College implements Serializable {

	private static final long serialVersionUID = -714986744436034530L;
	private Long collegeId;
	private String collegeName;
	public Long getCollegeId() {
		return collegeId;
	}
	public void setCollegeId(Long collegeId) {
		this.collegeId = collegeId;
	}
	public String getCollegeName() {
		return collegeName;
	}
	public void setCollegeName(String collegeName) {
		this.collegeName = collegeName;
	}
	//增加包含多个major的List
	private List<Major> majorList;

	public List<Major> getMajorList() {
		return majorList;
	}
	public void setMajorList(List<Major> majorList) {
		this.majorList = majorList;
	}
}
