package persistence.mybatis.model;

public class CollegeInfo {
	private Long collegeId;
	private Integer teacherCount;
	
	public CollegeInfo() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public CollegeInfo(Long collegeId, Integer teacherCount) {
		super();
		this.collegeId = collegeId;
		this.teacherCount = teacherCount;
	}

	public Long getCollegeId() {
		return collegeId;
	}
	public void setCollegeId(Long collegeId) {
		this.collegeId = collegeId;
	}
	public Integer getTeacherCount() {
		return teacherCount;
	}
	public void setTeacherCount(Integer teacherCount) {
		this.teacherCount = teacherCount;
	}
	
}
