package persistence.mybatis.mapper;

import java.util.List;

import persistence.mybatis.model.College;

public interface CollegeMapper {
	int deleteByPrimaryKey(Long collegeId);

	int insert(College record);

	College selectByPrimaryKey(Long collegeId);

	List<College> selectAll();

	int updateByPrimaryKey(College record);

	List<College> selectAllWithoutInnerJoin();
}
