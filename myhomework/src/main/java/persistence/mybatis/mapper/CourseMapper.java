package persistence.mybatis.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import persistence.mybatis.model.Course;

public interface CourseMapper {
	public List<Course> getAll();
	public List<Course> getCourseById(@Param("courseId") Long id);
	public int addCourse(Course c);
	public int delCourse(@Param("id") Long id);
	public int updCourse(@Param("courseId") Long id,@Param("modifiedCourse") Course modifedCourse);
	
	//以下用于示例动态SQL
	public List<Course> getAllOrById(@Param("courseId") Long id);
	public List<Course> getByIdByName(@Param("courseId") Long id,@Param("courseName") String name);
	public List<Course> getByIdByName2(@Param("courseId") Long id,@Param("courseName") String name);
	public int updCourse2(@Param("courseId") Long id,@Param("modifiedCourse") Course modifedCourse);
	public int updCourse3(@Param("courseId") Long id,@Param("modifiedCourse") Course modifedCourse);
	public List<Course> getCourseByIdIterable(@Param("ids") Iterable<Long> idIterable);
	public int addCourseList(@Param("courseList") List<Course> courses);
}
