package persistence.mybatis.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import persistence.mybatis.model.Teacher;
/*
 * 与TeacherDao方法完全一致，为了遵循Mybatis的习惯，这里叫做TeacherMapper
 * */
public interface TeacherMapper {
	public int insertTeacher(Teacher newTeacher);//没有使用@Param,所以在xml中直接写属性即可访问
	public int deleteTeacher(@Param("teacherId")Long teacherId);
	public int updateTeacher(@Param("mTeacher")Teacher modifiedTeacher);//需要在xml中写 mTeacher.属性
	public List<Teacher> queryTeachersByCollege(@Param("collegeId")Long collegeId);
	public Teacher findOneTeacherById(@Param("teacherId")Long teacherId);
	public int countTeacher();
}
