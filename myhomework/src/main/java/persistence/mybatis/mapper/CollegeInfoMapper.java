package persistence.mybatis.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import persistence.mybatis.model.CollegeInfo;

public interface CollegeInfoMapper {	
	public int updateByCollegeId(@Param("collegeId") Long collegeId,@Param("teacherCount") Integer teacherCount);
	public List<CollegeInfo> queryAllCollegeInfo();
	public CollegeInfo queryByCollegeId(@Param("collegeId") Long collegeId);
	public int increaseTeacherCount(@Param("collegeId") Long collegeId);
	public int decreaseTeacherCount(@Param("collegeId") Long collegeId);
}
