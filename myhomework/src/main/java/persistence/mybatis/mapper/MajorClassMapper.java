package persistence.mybatis.mapper;

import java.util.List;
import persistence.mybatis.model.MajorClass;

public interface MajorClassMapper {
    int deleteByPrimaryKey(Long classId);

    int insert(MajorClass record);

    MajorClass selectByPrimaryKey(Long classId);

    List<MajorClass> selectAll();

    int updateByPrimaryKey(MajorClass record);
    
    MajorClass selectByPrimaryKey_association(Long classId);//利用association标签来处理关联对象
    
    MajorClass selectByPrimaryKey_association_multisql(Long classId);//利用association标签来处理关联对象 多个Sql
}