package persistence.mybatis.mapper;

import java.util.List;
import persistence.mybatis.model.Major;

public interface MajorMapper {
    int deleteByPrimaryKey(Long majorId);

    int insert(Major record);

    Major selectByPrimaryKey(Long majorId);
    Major selectByPrimaryKey_flushCache(Long majorId);//配置文件中打开flushCache
    List<Major> selectAll();

    int updateByPrimaryKey(Major record);
    
    List<Major> selectMajorsByCollegeId(Long specifiedCollegeId);//查询指定院系的专业
}