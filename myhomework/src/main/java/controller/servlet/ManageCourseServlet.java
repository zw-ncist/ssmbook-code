package controller.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;

import persistence.mybatis.model.Course;
import business.*;

/**
 * Servlet implementation class ManageCourseServlet
 */
public class ManageCourseServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ManageCourseServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		ManageCourse mc=new ManageCourseImpl();
		String act = request.getParameter("act");

		
		String jsonStr = "";
		if (act.equalsIgnoreCase("getAll")) {
			List<Course> courseList = mc.getAllCourse();

			jsonStr = JSON.toJSONString(courseList);

		} else if (act.equalsIgnoreCase("getCourseById")) {
			Long id=Long.valueOf(request.getParameter("courseId"));
			List<Course> courseList=mc.queryCourseById(id);

			jsonStr = JSON.toJSONString(courseList);

		} else if (act.equalsIgnoreCase("addCourse")) {
			System.out.println(request.getParameter("courseName")+request.getParameter("courseScore"));
			
			Course newCourse=new Course();
			newCourse.setCourseName(request.getParameter("courseName"));
			newCourse.setCourseScore(Float.valueOf(request.getParameter("courseScore")));
			
			System.out.println(newCourse);
			
			mc.addNewCourse(newCourse);
			jsonStr="{'re':'Success'}";
			
		} else if (act.equalsIgnoreCase("delCourse")) {
			Long id=Long.valueOf(request.getParameter("courseId"));
			mc.delCourse(id);
			jsonStr="{'re':'Success'}";
			
		} else if (act.equalsIgnoreCase("updCourse")) {
			Course newCourse=new Course();
			newCourse.setCourseId(Long.valueOf(request.getParameter("courseId")));
			newCourse.setCourseName(request.getParameter("courseName"));
			newCourse.setCourseScore(Float.valueOf(request.getParameter("courseScore")));
			mc.modifyCourse(newCourse);

		}
		response.setContentType("text/json;charset=UTF-8");
		PrintWriter out = response.getWriter();
		out.println(jsonStr);
		out.flush();
		out.close();

	}

}
