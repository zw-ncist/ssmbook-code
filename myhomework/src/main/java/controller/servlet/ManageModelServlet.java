package controller.servlet;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.ibatis.session.SqlSession;

import persistence.mybatis.mapper.CollegeMapper;
import persistence.mybatis.model.MyBatisUtils;

/**
 * Servlet implementation class ManageModelServlet
 */
public class ManageModelServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private String mapper_package="";
    private String mapper_suffix="";
    @Override
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
		super.init(config);
		this.mapper_package=config.getInitParameter("mapper_package");//初始化模型的包前缀
		this.mapper_suffix=config.getInitParameter("mapper_suffix");//初始化DAO接口的后缀名  例如Mapper
	}

	/**
     * @see HttpServlet#HttpServlet()
     */
    public ManageModelServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String class_name=request.getParameter("entity").toLowerCase();
		class_name=class_name.substring(0, 0).toUpperCase()+class_name.substring(1);//格式化好类名
		
		String mapper_name = this.mapper_package+"."+class_name+this.mapper_suffix;//构建字符串  persistence.mybatis.mapper.CollegeMapper
		SqlSession sqlSession = MyBatisUtils.getSession();
		
		//List<College> collegeList=session.selectList("selectAll");直接用sssion发送SQL的方式，不建议使用了
		Class mapperClass=null;
		
		try {
			mapperClass = Class.forName(mapper_name);//从字符串得到class对象
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Object mapper= sqlSession.getMapper(mapperClass);
		
	}

}
