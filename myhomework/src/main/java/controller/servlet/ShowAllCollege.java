package controller.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.ibatis.session.SqlSession;

import com.alibaba.fastjson.JSON;

import persistence.mybatis.mapper.CollegeMapper;
import persistence.mybatis.model.College;
import persistence.mybatis.model.MyBatisUtils;

/**
 * Servlet implementation class ShowAllCollege
 */
public class ShowAllCollege extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ShowAllCollege() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		SqlSession sqlSession = MyBatisUtils.getSession();
		//List<College> collegeList=session.selectList("selectAll");直接用sssion发送SQL的方式，不建议使用了

		CollegeMapper mapper = sqlSession.getMapper(CollegeMapper.class);

		List<College> collegeList = mapper.selectAll();
		// 使用Mapper发送SQL是常见的做法，也更容易和Spring集成
		sqlSession.close();
		String jsonStr = JSON.toJSONString(collegeList);

		response.setContentType("text/json;charset=UTF-8");

		PrintWriter out = response.getWriter();
		out.println(jsonStr);
		out.flush();
		out.close();
	}

}

