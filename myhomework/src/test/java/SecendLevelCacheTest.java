import static org.junit.Assert.*;

import java.util.List;
import org.apache.ibatis.session.SqlSession;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

import persistence.mybatis.mapper.MajorMapper;
import persistence.mybatis.model.Major;
import persistence.mybatis.model.MyBatisUtils;

import static org.hamcrest.Matchers.*;

//定义测试方法的顺序，按照名称升序
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SecendLevelCacheTest {

	@Test
	public void test1_one_SqlSession() {

		SqlSession sqlSession = MyBatisUtils.getSession();
		MajorMapper mapper = (MajorMapper) sqlSession.getMapper(MajorMapper.class);

		Major result1 = mapper.selectByPrimaryKey(1L);// 对象在一级缓存。会看到二级缓存命中率为0，没有该对象
		System.out.println("*****result1=" + result1);

		Major result2 = mapper.selectByPrimaryKey(1L);// 对象在一级缓存。会看到二级缓存命中率为0，没有该对象
		System.out.println("*****result2=" + result2);

		assertThat(result2, is(result1));// 同一个对象

		sqlSession.commit();// commit后才会进入二级缓存,
		// 仍然按照 二级缓存-一级缓存-数据库的顺序

		// 二级缓存里已经存在对象了
		Major result3 = mapper.selectByPrimaryKey(1L);// 命中二级缓存
//		assertThat(result3, is(result1));//readOnly=True时，result3与result1是一个对象
		assertThat(result3, not(result1));//readOnly=false时，result3与result1不是一个对象
		System.out.println("*****result3=" + result3);
		
		Major result4 = mapper.selectByPrimaryKey(1L);// 命中二级缓存
//		assertThat(result4, is(result3));//readOnly=True时，result4与result1是一个对象
		assertThat(result4, not(result3));//readOnly=false时，result4与result1不是一个对象
		System.out.println("*****result4=" + result4);

		sqlSession.close();

	}

	@Test
	public void test2_two_SqlSession() {

		SqlSession sqlSession = MyBatisUtils.getSession();
		MajorMapper mapper = (MajorMapper) sqlSession.getMapper(MajorMapper.class);

		Major result1 = mapper.selectByPrimaryKey(1L);// 对象在一级缓存。会看到二级缓存命中率为0，没有该对象
		System.out.println("*****result1=" + result1);

		Major result2 = mapper.selectByPrimaryKey(1L);// 对象在一级缓存。会看到二级缓存命中率为0，没有该对象
		System.out.println("*****result2=" + result2);

		assertThat(result2, is(result1));// 同一个对象

		sqlSession.close();
//		sqlSession.commit();//commit或close后才会进入二级缓存,
		// 仍然按照 二级缓存-一级缓存-数据库的顺序

		sqlSession = MyBatisUtils.getSession();
		mapper = (MajorMapper) sqlSession.getMapper(MajorMapper.class);
		// 二级缓存里已经存在对象了
		Major result3 = mapper.selectByPrimaryKey(1L);// 命中二级缓存
//		assertThat(result3, is(result1));// readOnly=True时，result3与result1是一个对象
		assertThat(result3, not(result1));// readOnly=false时，result3与result1不是一个对象
		System.out.println("*****result3=" + result3);

		Major result4 = mapper.selectByPrimaryKey(1L);// 命中二级缓存
//		assertThat(result4, is(result3));// readOnly=True时，result4与result1是一个对象
		assertThat(result4, not(result3));// readOnly=false时，result4与result1不是一个对象
		System.out.println("*****result4=" + result4);

		sqlSession.close();
	}

	

	@Test
	public void test3_one_SqlSession() {

		SqlSession sqlSession = MyBatisUtils.getSession();
		MajorMapper mapper = (MajorMapper) sqlSession.getMapper(MajorMapper.class);

		Major result1 =mapper.selectByPrimaryKey(1L);//Cache Hit Ratio=0/1二级没有，无法命中。一级也没有，所以需要从数据库读取。存入一级
		System.out.println("*****result1=" + result1);
		
		Major result11 =mapper.selectByPrimaryKey(1L);//二级没有，无法命中  Cache Hit Ratio=0/2
		System.out.println("*****result11=" + result11);
		
		sqlSession.commit();//一级更新到二级
		Major result2 =mapper.selectByPrimaryKey(1L);//二级命中  Cache Hit Ratio=1/3
		System.out.println("*****result2=" + result2);
		
		//接下来执行一个插入
		Major newMajor=new Major();
		newMajor.setMajorName("物联网工程");
		newMajor.setCollegeId(1L);
		newMajor.setMajorId(null);
		int re = mapper.insert(newMajor);//清空了二级和一级缓存
		sqlSession.commit();//一级更新到二级
		assertThat(re, is(1));
		System.out.println("+++++++++++++++insert major+++++++++++++++++++");
		
		Major result3 =mapper.selectByPrimaryKey(1L);//向数据库检索   Cache Hit Ratio=1/4
		System.out.println("*****result3=" + result3);
		
		
		re=mapper.deleteByPrimaryKey(newMajor.getMajorId());//清空了二级和一级缓存
		sqlSession.commit();//一级更新到二级，但一级本身也没有
		assertThat(re, is(1));
		System.out.println("++++++++++++++++delete major++++++++++++++++++");

		
		Major result4 =mapper.selectByPrimaryKey(1L);//二级没有，无法命中Cache Hit Ratio=1/5。一级也没有，所以需要从数据库读取。存入一级  
		System.out.println("*****result4=" + result4);
		sqlSession.commit();
		
		Major  result5 =mapper.selectByPrimaryKey(1L);//二级命中，Cache Hit Ratio=2/6。 Cache Hit Ratio=2/5
		System.out.println("*****result5=" + result5);
		sqlSession.close();

	}

}
