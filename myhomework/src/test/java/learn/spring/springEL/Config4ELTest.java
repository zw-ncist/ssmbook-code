package learn.spring.springEL;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.*;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import learn.spring.assemble.Bicycle;

public class Config4ELTest {

	@Test
	public void test() {
		ApplicationContext ac=new AnnotationConfigApplicationContext (learn.spring.springEL.Config4EL.class);
		Component4EL c=(Component4EL)ac.getBean("component4EL");
		assertThat(c,notNullValue());
		System.out.println(c.toString());
	}

}
