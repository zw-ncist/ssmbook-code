package learn.spring.aop;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.*;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import learn.spring.aop.service.TimeStampComponent;
import learn.spring.aop.service.UserService;

public class AopTest {

//	@Test
	public void testRecordAspectByAnnotation() throws Exception {
		ApplicationContext ac=new AnnotationConfigApplicationContext (learn.spring.aop.Config4Aop.class);
		UserService us=(UserService)ac.getBean("userService");
		assertThat(us,notNullValue());		
		us.verify("tom", "123");	
////		us.verify("tom", "456123");	
////		us.verify("", "123");
		us.verify("tom", "");
		us.verify("admin", "123");

	}

//	@Test
	public void testRecordAspectByXML() throws Exception {
		ApplicationContext ac=new ClassPathXmlApplicationContext  ("learn/spring/aop/aop_xml.xml");
		UserService us=(UserService)ac.getBean("userService");
		assertThat(us,notNullValue());
		us.verify("tom", "123");	
////		us.verify("tom", "456123");	
////		us.verify("", "123");
		us.verify("tom", "");
		us.verify("admin", "123");

	}
	
	@Test 
	public void testIntroduction() {
//		ApplicationContext ac=new AnnotationConfigApplicationContext (learn.spring.aop.Config4Aop.class);//使用配置类
		ApplicationContext ac=new ClassPathXmlApplicationContext  ("learn/spring/aop/aop_xml.xml");//使用配置文件
		UserService us=(UserService)ac.getBean("userService");
		TimeStampComponent tsc=(TimeStampComponent)us;
		String re=tsc.addTimeStamp("tom");
		System.out.println(re);
	}

}
