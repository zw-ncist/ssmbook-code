package learn.spring.aop;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.*;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import learn.spring.aop.service.TimeStampComponent;
import learn.spring.aop.service.UserService;
import learn.spring.aop.service.UserServiceImpl;
import learn.spring.assemble.Bicycle;

public class AopTest2 {

	@Test
	public void testAdvancedAspectByAnnotation() throws Exception {
		ApplicationContext ac=new AnnotationConfigApplicationContext (learn.spring.aop.Config4Aop.class);
		System.out.println("---------userService-----------");
		UserService us=(UserService)ac.getBean("userService");
		us.verify("tom", "123");	
		System.out.println("---------manager-----------");
		Manager m=(Manager)ac.getBean("manager");
		m.beFired();
		System.out.println("---------manager-----------");
		m.checkUserService(us);
		System.out.println("----------subManager----------");
		SubManager subm=(SubManager)ac.getBean("subManager");
		subm.beFired();
		System.out.println("---------subManager-----------");
		subm.beEmployed();
//		System.out.println("--------------------");
//		SubSubManager subsubm=(SubSubManager)ac.getBean("subSubManager");
//		subsubm.beFired();
	}


}
