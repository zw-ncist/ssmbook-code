package learn.spring.assemble;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.*;

import java.awt.Color;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import learn.spring.ioc.Car;

public class AssembleBeansTest {

	@Test
	public void byXMLtest() {//普通xml
		ApplicationContext ac=new ClassPathXmlApplicationContext ("learn/spring/assemble/xml/just_xml.xml");
		Bicycle bicycle=(Bicycle)ac.getBean("bicycle");
		assertThat(bicycle,notNullValue());
		System.out.println(bicycle.toString());
	}

	@Test
	public void byXML_NameSpacetest() {//带有c命名空间和p命名空间的xml
		ApplicationContext ac=new ClassPathXmlApplicationContext ("learn/spring/assemble/xml/just_xml_namespace.xml");
		Bicycle bicycle=(Bicycle)ac.getBean("bicycle");
		assertThat(bicycle,notNullValue());
		System.out.println(bicycle.toString());
	}
	
	@Test
	public void byJavaConfig() {//使用配置类
		ApplicationContext ac=new AnnotationConfigApplicationContext (learn.spring.assemble.Config4Bicycle.class);
		Bicycle bicycle=(Bicycle)ac.getBean("bicycle");
		assertThat(bicycle,notNullValue());
		System.out.println(bicycle.toString());
	}
	@Test
	public void byJavaConfigAndXML() {//配置类里含有对xml配置的导入
		ApplicationContext ac=new AnnotationConfigApplicationContext (learn.spring.assemble.PartConfig4Bicycle.class);
		Bicycle bicycle=(Bicycle)ac.getBean("bicycle");
		assertThat(bicycle,notNullValue());
		System.out.println(bicycle.toString());
	}
	@Test
	public void byXML_NameSpacetestAndJavaConfig() {//使用xml配置Spring,并引入配置类中的bean
		ApplicationContext ac=new ClassPathXmlApplicationContext ("learn/spring/assemble/xml/part_xml2.xml");
		Bicycle bicycle=(Bicycle)ac.getBean("bicycle");
		assertThat(bicycle,notNullValue());
		System.out.println(bicycle.toString());	
	}
}
