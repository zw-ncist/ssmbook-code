package learn.spring.transaction;

import java.util.Date;
import java.util.Random;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import business.ManageTeacher;

public class MultiThreadTransactionTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ApplicationContext ac = new AnnotationConfigApplicationContext(
				learn.spring.transaction.Config4SpringMybatisTransaction.class);
		ManageTeacher mt = ac.getBean(ManageTeacher.class);
//	Long teacherId = mt.createNewTeacher(1L, "Tom Cruise" + new Date().getTime());
		/*
		 * 写一个8多线程的程序，每个线程创建1个老师，最大设为8。看看结果
		 */
		int k = 0;
		for (; k < 8; k++) {
			System.out.println("启动线程" + k);
			new Thread(() -> {
				Random r = new Random();
				Long id1 = mt.createNewTeacher(1L, "Thread Teacher" + r.nextInt());
				System.out.println("Thread  Teacher=" + id1);
				
			}).start();
		}

	}

}
