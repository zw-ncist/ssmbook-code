package learn.spring.transaction;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.util.Date;

import org.apache.ibatis.session.SqlSessionFactory;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import business.ManageTeacher;
import persistence.mybatis.mapper.CollegeInfoMapper;
import persistence.mybatis.model.CollegeInfo;

public class TestConfig4SpringMybatisTransaction {

//	@Test
	public void testCollegeInfoMapper() {

		int re = 0;
		ApplicationContext ac = new AnnotationConfigApplicationContext(
				learn.spring.transaction.Config4SpringMybatisTransaction.class);
		SqlSessionFactory sqlSessionFactory = (SqlSessionFactory) ac.getBean("sqlSessionFactory");
		assertThat(sqlSessionFactory, notNullValue());
		CollegeInfoMapper cm = ac.getBean(CollegeInfoMapper.class);// 都可以
		assertThat(cm, notNullValue());
		System.out.println(cm.queryAllCollegeInfo());// 查询结果
		re = 0;
		re = cm.updateByCollegeId(1L, 5000);
		assertThat(re, equalTo(1));// 应当更新1条记录
		CollegeInfo ci = cm.queryByCollegeId(1L);
		assertThat(ci.getTeacherCount(), equalTo(5000));// 查询一下
		re = 0;
		re = cm.updateByCollegeId(1L, 10); // 再该回去
		assertThat(re, equalTo(1));// 应当更新1条记录

	}

	@Test
	public void testManageTeacherService() {
		/*
		 * 设计个事务场景
		 */
		int re = 0;
		ApplicationContext ac = new AnnotationConfigApplicationContext(
				learn.spring.transaction.Config4SpringMybatisTransaction.class);
		ManageTeacher mt = ac.getBean(ManageTeacher.class);
		Long teacherId = mt.createNewTeacher(1L, "Tom Cruise" + new Date().getTime());
		assertThat(teacherId, notNullValue());// 应当更新1条记录

	}

}
