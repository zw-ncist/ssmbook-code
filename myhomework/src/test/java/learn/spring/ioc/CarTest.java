package learn.spring.ioc;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class CarTest {

	@Test
	public void normal() {
		Motor motor=new Motor(2.0f,4);//需要自己构建对象
		Car mycar=new Car();
		mycar.setModelName("梦想汽车");
		mycar.setMotor(motor);//需要自己完成依赖的引用
		System.out.println(mycar.toString());
	}
	
	@Test
	public void ioc_test() {
		ApplicationContext ac=new ClassPathXmlApplicationContext ("car-config.xml");
		Car car=(Car)ac.getBean("car");
		System.out.println(car.toString());
	}
	@Test
	public void bean_lifecycle() {
		ClassPathXmlApplicationContext ac=new ClassPathXmlApplicationContext ("car-config.xml");
		Wheel wheel=(Wheel)ac.getBean("wheel");
		System.out.println(wheel.toString());
		ac.close();//容器关闭
	}
}
