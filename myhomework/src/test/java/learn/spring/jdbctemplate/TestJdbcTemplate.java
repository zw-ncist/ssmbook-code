package learn.spring.jdbctemplate;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.*;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import learn.spring.assemble.Bicycle;
import persistence.mybatis.model.Teacher;

public class TestJdbcTemplate {

	@Test
	public void testTeacherDao() {
		int re=0;
		ApplicationContext ac=new AnnotationConfigApplicationContext (learn.spring.jdbctemplate.Config4JDBCTemplate.class);
		TeacherDao td=(TeacherDao)ac.getBean("teacherDao");
		assertThat(td,notNullValue());
		System.out.println(td.countTeacher());//查询总数
		
		System.out.println(td.queryTeachersByCollege(2L));//查询多个记录
		
		Teacher newTeacher=new Teacher();//插入一个新纪录
		newTeacher.setTeacherId(null);
		newTeacher.setCollegeId(1L);
		newTeacher.setTeacherName("曹操");
		re=0;
		re=td.insertTeacher(newTeacher);//插入
		assertThat(re,equalTo(1));//应当插入1条记录
		assertThat(newTeacher.getTeacherId(),notNullValue());//应当取得自增的主键
		System.out.println(newTeacher);
		
		newTeacher.setTeacherName("刘备");
		newTeacher.setCollegeId(2L);
		re=0;
		re=td.updateTeacher(newTeacher);//修改
		assertThat(re,equalTo(1));//应当更新1条记录
		System.out.println(newTeacher);
		
		Teacher findTeacher=td.findOneTeacherById(newTeacher.getTeacherId());//查询一个记录
		assertThat(findTeacher,notNullValue());
		System.out.println(findTeacher);
		
		re=0;
		re=td.deleteTeacher(newTeacher.getTeacherId());
		assertThat(re,equalTo(1));//应当删除1条记录
		
		
	}

}
