package persistence.mybatis.mapper;

import static org.junit.Assert.*;

import java.util.List;
import org.apache.ibatis.session.SqlSession;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

import persistence.mybatis.model.Major;
import persistence.mybatis.model.MajorClass;
import persistence.mybatis.model.MyBatisUtils;

import static org.hamcrest.Matchers.*;

//定义测试方法的顺序，按照名称升序
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MajorClassMapperTest {
	MajorClassMapper mapper = null;
	SqlSession sqlSession = null;
	public static Long majorId = null;
	// 设置为静态，否则无法在测是方法间共享

	// 每个测试方法之前执行
	@Before
	public void before() {
		this.sqlSession = MyBatisUtils.getSession();
		this.mapper = (MajorClassMapper) sqlSession.getMapper(MajorClassMapper.class);
	}

	// 每个测试方法之后执行
	@After
	public void after() {

		this.sqlSession.commit();
		this.sqlSession.close();
	}

	@Test
	public void test1_selectAll() {
		List<MajorClass> result = this.mapper.selectAll();
		assertThat(result.size(), greaterThanOrEqualTo(5));
		for(MajorClass mc:result) {
			assertThat(mc.getMajor(), notNullValue());
			System.out.println(mc.getClassName()+" -> "+mc.getMajor().getMajorName());
		}
	}

	@Test
	public void test2_selectByPrimaryKey() {
		MajorClass majorClass = this.mapper.selectByPrimaryKey(1L);
		assertThat(majorClass, notNullValue());
		assertThat(majorClass.getMajor(), notNullValue());
		System.out.println(majorClass.getClassName()+" -> "+majorClass.getMajor().getMajorName());
	}

	@Test
	public void test3_selectByPrimaryKey_association() {
		MajorClass majorClass = this.mapper.selectByPrimaryKey_association(1L);
		assertThat(majorClass, notNullValue());
		assertThat(majorClass.getMajor(), notNullValue());
		System.out.println(majorClass.getClassName()+" -> "+majorClass.getMajor().getMajorName());
	}
	@Test
	public void test3_selectByPrimaryKey_association_multisql() {
		MajorClass majorClass = this.mapper.selectByPrimaryKey_association_multisql(1L);
		assertThat(majorClass, notNullValue());
		assertThat(majorClass.getMajor(), notNullValue());
		System.out.println(majorClass.getClassName()+" -> "+majorClass.getMajor().getMajorName());
	}


}
