package persistence.mybatis.mapper;

import static org.junit.Assert.*;

import java.util.List;
import org.apache.ibatis.session.SqlSession;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

import persistence.mybatis.model.College;
import persistence.mybatis.model.Major;
import persistence.mybatis.model.MyBatisUtils;

import static org.hamcrest.Matchers.*;

//定义测试方法的顺序，按照名称升序
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CollegeMapperTest {
	CollegeMapper mapper = null;
	SqlSession sqlSession = null;
	public static Long entityId = null;
	// 设置为静态，否则无法在测是方法间共享

	// 每个测试方法之前执行
	@Before
	public void before() {
		this.sqlSession = MyBatisUtils.getSession();
		this.mapper = (CollegeMapper) sqlSession.getMapper(CollegeMapper.class);
	}

	// 每个测试方法之后执行
	@After
	public void after() {

		this.sqlSession.commit();
		this.sqlSession.close();
	}

	@Test
	public void test1_selectAll() {
		List<College> result = this.mapper.selectAll();
		assertThat(result.size(), greaterThanOrEqualTo(3));
		int count = 0;
		// 为了显示一下
		for (College college : result) {
			for (Major m : college.getMajorList()) {
				System.out.println(m.getMajorId() + "======" + m.getMajorName()+" "+count);
				count++;
			}
		}
		assertThat(count, is(8));
	}

	@Test
	public void test1_selectAllWithoutInnerJoin() {
		List<College> result = this.mapper.selectAllWithoutInnerJoin();
		assertThat(result.size(), greaterThanOrEqualTo(3));
		int count = 0;
		// 为了显示一下
		for (College college : result) {
			for (Major m : college.getMajorList()) {
				System.out.println(m.getMajorId() + "********" + m.getMajorName());
				count++;
			}
		}
		assertThat(count, is(8));
	}
	@Test
	public void test2_selectByPrimaryKey() {
		College college = this.mapper.selectByPrimaryKey(1L);
		assertThat(college, notNullValue());

		// 为了显示一下
		for (Major m : college.getMajorList()) {
			System.out.println(m.getMajorId() + "  " + m.getMajorName());
		}
		assertThat(college.getMajorList().size(), is(3));
	}

	@Test
	public void test3_insert() {
		College newEntity = new College();
		newEntity.setCollegeName("大数据学院");
		newEntity.setCollegeId(null);

		int re = this.mapper.insert(newEntity);
		assertThat(re, is(1));
		entityId = newEntity.getCollegeId();
		// 为了直观看到效果，加入了一些控制台输出
		System.out.println("added ObjectID=" + entityId);
	}

	@Test
	public void test4_updateByPrimaryKey() {
		College newEntity = new College();
		newEntity.setCollegeName("人工智能学院");
		newEntity.setCollegeId(entityId);

		int re = this.mapper.updateByPrimaryKey(newEntity);
		assertThat(re, is(1));
	}

	@Test
	public void test5_deleteByPrimaryKey() {
		int re = this.mapper.deleteByPrimaryKey(entityId);
		assertThat(re, is(1));
	}

}
