package persistence.mybatis.mapper;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.ibatis.session.SqlSession;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

import persistence.mybatis.model.Course;
import persistence.mybatis.model.MyBatisUtils;

import static org.hamcrest.Matchers.*;

//定义测试方法的顺序，按照名称升序
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CourseMapperTest {
	CourseMapper mapper=null;
	SqlSession sqlSession=null;
	public static Long courseId=null;
	//设置为静态，否则无法在测是方法间共享
	
	// 每个测试方法之前执行
	@Before
	public void before() {
		this.sqlSession = MyBatisUtils.getSession();
		this.mapper = (CourseMapper) sqlSession.getMapper(CourseMapper.class);
	}

	// 每个测试方法之后执行
	@After
	public void after() {
		
		this.sqlSession.commit();
		this.sqlSession.close();
	}

	@Test
	public void test2GetAll() {
		List<Course> result=this.mapper.getAll();
		//为了直观看到效果，加入了一些控制台输出
		System.out.println("数据库检索结果：");
		for(Course c :result){
			System.out.println(c);
		}
	}

	@Test
	public void test4GetCourseById() {
		List<Course> result=this.mapper.getCourseById(this.courseId);
		assertThat(result.size(),greaterThanOrEqualTo(1));
		
		//为了直观看到效果，加入了一些控制台输出
		System.out.println("Fetched Courses:");
		for(Course c :result){
			System.out.println(c);
		}
	}
	
	@Test
	public void test4GetCourseByIdIterable() {
		List<Long> idList=new ArrayList<Long>();
		idList.add(1L);
		idList.add(2L);
		List<Course> result=this.mapper.getCourseByIdIterable(idList);
		assertThat(result.size(),greaterThanOrEqualTo(2));
		
		Set<Long> idSet=new HashSet<Long>();
		idSet.add(1L);
		idSet.add(2L);
		List<Course> result2=this.mapper.getCourseByIdIterable(idSet);
		assertThat(result2.size(),greaterThanOrEqualTo(2));
	}


	@Test
	public void test1AddCourse() {
		Course newCourse=new Course();
		newCourse.setCourseName("JavaEE");
		newCourse.setCourseScore(2.5);
		int re=this.mapper.addCourse(newCourse);
		assertThat(re,is(1));
		courseId=newCourse.getCourseId();
		//为了直观看到效果，加入了一些控制台输出
		System.out.println("added="+newCourse);
	}

	@Test
	public void test1AddCourses() {
		
		List<Course> courses=new ArrayList<Course>();
		
		Course newCourse=new Course();
		newCourse.setCourseName("Python");
		newCourse.setCourseScore(2.5);
		courses.add(newCourse);
		
		newCourse=new Course();
		newCourse.setCourseName("C#");
		newCourse.setCourseScore(1.5);
		courses.add(newCourse);
		
		
		int re=this.mapper.addCourseList(courses);
		assertThat(re,is(2));
		
		for (Course c : courses){
		    System.out.println("**************CourseId="+c.getCourseId()+"   CourseName="+c.getCourseName());
		    this.mapper.delCourse(c.getCourseId());//del
		}
	}
	
	@Test
	public void test5DelCourse() {
		int re=this.mapper.delCourse(courseId);
		assertThat(re,is(1));
		//在控制台输出一下
		test2GetAll();
	}

	@Test
	public void test3UpdCourse() {
		System.out.println(courseId);
		Course modifiedCourse=new Course();
		modifiedCourse.setCourseName("JavaEE高级编程");
		modifiedCourse.setCourseScore(5);
		int re=this.mapper.updCourse(courseId,modifiedCourse);
		assertThat(re,is(1));
		//为了直观看到效果，加入了一些控制台输出
		test2GetAll();
	}

	@Test
	public void test6GetAllOrById() {
		List<Course> result=this.mapper.getAllOrById(1L);
		assertThat(result.size(),greaterThanOrEqualTo(1));
		
		
		Long id=null;
		List<Course> result1=this.mapper.getAllOrById(id);
		assertThat(result1.size(),greaterThanOrEqualTo(2));
		
	}
	@Test
	public void test7GetByIdByName() {
		List<Course> result=this.mapper.getByIdByName(1L, null);
		assertThat(result.size(),greaterThanOrEqualTo(1));
		
		List<Course> result1=this.mapper.getByIdByName(null,"J");
		assertThat(result1.size(),greaterThanOrEqualTo(2));

		List<Course> result2=this.mapper.getByIdByName(1L,"J");
		assertThat(result2.size(),greaterThanOrEqualTo(1));
		
		List<Course> result3=this.mapper.getByIdByName(null,null);
		assertThat(result3.size(),greaterThanOrEqualTo(0));
		
	}
	@Test
	public void test8GetByIdByName2() {
		List<Course> result=this.mapper.getByIdByName2(1L, null);
		assertThat(result.size(),greaterThanOrEqualTo(1));
		
		List<Course> result1=this.mapper.getByIdByName2(null,"J");
		assertThat(result1.size(),greaterThanOrEqualTo(2));

		List<Course> result2=this.mapper.getByIdByName2(1L,"J");
		assertThat(result2.size(),greaterThanOrEqualTo(1));
		
		List<Course> result3=this.mapper.getByIdByName2(null,null);
		assertThat(result3.size(),greaterThanOrEqualTo(0));
		
	}
	@Test
	public void test9UpdCourse2() {
		
		Course modifiedCourse=new Course();
		modifiedCourse.setCourseName("JavaEE高级编程");
		modifiedCourse.setCourseScore(5);
		int re=this.mapper.updCourse2(2L,modifiedCourse);
		assertThat(re,is(1));


		modifiedCourse.setCourseName("JavaEE");
		modifiedCourse.setCourseScore(2.0);
		int re2=this.mapper.updCourse2(2L,modifiedCourse);
		assertThat(re2,is(1));
		
	}
	
	@Test
	public void test9UpdCourse3() {
		
		Course modifiedCourse=new Course();
		modifiedCourse.setCourseName("JavaEE高级编程");
		modifiedCourse.setCourseScore(5);
		int re=this.mapper.updCourse3(2L,modifiedCourse);
		assertThat(re,is(1));


		modifiedCourse.setCourseName("JavaEE");
		modifiedCourse.setCourseScore(2.0);
		int re2=this.mapper.updCourse3(2L,modifiedCourse);
		assertThat(re2,is(1));
	}

}
