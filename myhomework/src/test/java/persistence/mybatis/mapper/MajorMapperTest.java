package persistence.mybatis.mapper;

import static org.junit.Assert.*;

import java.util.List;
import org.apache.ibatis.session.SqlSession;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

import persistence.mybatis.model.Major;
import persistence.mybatis.model.MyBatisUtils;

import static org.hamcrest.Matchers.*;

//定义测试方法的顺序，按照名称升序
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MajorMapperTest {
	MajorMapper mapper = null;
	SqlSession sqlSession = null;
	public static Long majorId = null;
	// 设置为静态，否则无法在测是方法间共享

	// 每个测试方法之前执行
	@Before
	public void before() {
		this.sqlSession = MyBatisUtils.getSession();
		this.mapper = (MajorMapper) sqlSession.getMapper(MajorMapper.class);
	}

	// 每个测试方法之后执行
	@After
	public void after() {

		this.sqlSession.commit();
		this.sqlSession.close();
	}

	@Test
	public void test1_selectAll() {
		List<Major> result = this.mapper.selectAll();
		assertThat(result.size(), greaterThanOrEqualTo(8));
	}

	@Test
	public void test2_selectByPrimaryKey() {
		Major major = this.mapper.selectByPrimaryKey(1L);
		assertThat(major, notNullValue());
	}

	@Test
	public void test3_insert() {
		Major newMajor=new Major();
		newMajor.setMajorName("物联网工程");
		newMajor.setCollegeId(1L);
		newMajor.setMajorId(null);

		int re = this.mapper.insert(newMajor);
		assertThat(re, is(1));
		majorId = newMajor.getMajorId();
		// 为了直观看到效果，加入了一些控制台输出
		System.out.println("added MajorId=" + majorId);
	}

	@Test
	public void test4_updateByPrimaryKey() {
		Major newMajor=new Major();
		newMajor.setMajorName("物联网工程与技术");
		newMajor.setCollegeId(1L);
		newMajor.setMajorId(majorId);
		
		int re=this.mapper.updateByPrimaryKey(newMajor);
		assertThat(re,is(1));
	}


	@Test
	public void test5_deleteByPrimaryKey() {
		int re=this.mapper.deleteByPrimaryKey(majorId);
		assertThat(re,is(1));
	}


}
