import static org.junit.Assert.*;

import java.util.List;
import org.apache.ibatis.session.SqlSession;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

import persistence.mybatis.mapper.MajorMapper;
import persistence.mybatis.model.Major;
import persistence.mybatis.model.MyBatisUtils;

import static org.hamcrest.Matchers.*;

//定义测试方法的顺序，按照名称升序
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class FirstLevelCacheTest {
	@Test
	public void test0_one_SqlSession() {

		SqlSession sqlSession = MyBatisUtils.getSession();
		MajorMapper mapper = (MajorMapper) sqlSession.getMapper(MajorMapper.class);

		Major result1 =mapper.selectByPrimaryKey_flushCache(1L);
		Major result2 =mapper.selectByPrimaryKey_flushCache(1L);//强制清空缓存
		assertThat(result2, not(result1));
		sqlSession.commit();
		sqlSession.close();

	}
	@Test
	public void test1_one_SqlSession() {

		SqlSession sqlSession = MyBatisUtils.getSession();
		MajorMapper mapper = (MajorMapper) sqlSession.getMapper(MajorMapper.class);

		Major result1 =mapper.selectByPrimaryKey(1L);//执行SQL
		Major result2 =mapper.selectByPrimaryKey(1L);//使用一级缓存
		assertThat(result2, is(result1));
		sqlSession.commit();
		sqlSession.close();

	}
	@Test
	public void test2_one_SqlSession() {

		SqlSession sqlSession = MyBatisUtils.getSession();
		MajorMapper mapper = (MajorMapper) sqlSession.getMapper(MajorMapper.class);
		Major result1 =mapper.selectByPrimaryKey(1L);
	
	
		sqlSession.commit();//清空了一级缓存
		
	
		Major result2 =mapper.selectByPrimaryKey(1L);
		assertThat(result2, not(result1));
		sqlSession.close();

	}
	@Test
	public void test3_one_SqlSession() {

		SqlSession sqlSession = MyBatisUtils.getSession();
		MajorMapper mapper = (MajorMapper) sqlSession.getMapper(MajorMapper.class);

		Major result1 =mapper.selectByPrimaryKey(1L);
		
		
		//接下来执行一个插入
		Major newMajor=new Major();
		newMajor.setMajorName("物联网工程");
		newMajor.setCollegeId(1L);
		newMajor.setMajorId(null);
		int re = mapper.insert(newMajor);//清空了一级缓存
		assertThat(re, is(1));
		
		
		Major result2 =mapper.selectByPrimaryKey(1L);
		assertThat(result2, not(result1));//不是一个对象
		sqlSession.close();

	}
	@Test
	public void test4_one_SqlSession() {

		SqlSession sqlSession = MyBatisUtils.getSession();
		MajorMapper mapper = (MajorMapper) sqlSession.getMapper(MajorMapper.class);

		Major result1 =mapper.selectByPrimaryKey(1L);
		
		
		//接下来执行一个更新
		Major newMajor=new Major();
		newMajor.setMajorName("计算机科学与技术");
		newMajor.setCollegeId(1L);
		newMajor.setMajorId(2L);
		int re=mapper.updateByPrimaryKey(newMajor);//清空了一级缓存
		assertThat(re,is(1));
		
		
		Major result2 =mapper.selectByPrimaryKey(1L);
		assertThat(result2, not(result1));//不是一个对象
		sqlSession.close();

	}
	
	@Test
	public void test5_one_SqlSession() {

		SqlSession sqlSession = MyBatisUtils.getSession();
		MajorMapper mapper = (MajorMapper) sqlSession.getMapper(MajorMapper.class);
		//准备一个数据
		Major newMajor=new Major();
		newMajor.setMajorName("物联网工程");
		newMajor.setCollegeId(1L);
		newMajor.setMajorId(null);
		int re = mapper.insert(newMajor);
		assertThat(re,is(1));
		
		//查询一个数据
		Major result1 =mapper.selectByPrimaryKey(1L);
		
		
		//接下来执行一个删除
		re=mapper.deleteByPrimaryKey(newMajor.getMajorId());//清空了一级缓存
		assertThat(re,is(1));
		
		
		Major result2 =mapper.selectByPrimaryKey(1L);
		assertThat(result2, not(result1));//不是一个对象
		

		sqlSession.close();

	}
	
	
	@Test
	public void test6_two_SqlSession() {

		SqlSession sqlSession = MyBatisUtils.getSession();
		MajorMapper mapper = (MajorMapper) sqlSession.getMapper(MajorMapper.class);
		Major result1 =mapper.selectByPrimaryKey(1L);
		sqlSession.close();
		
		sqlSession = MyBatisUtils.getSession();//一个新的sqlSession，一个新的一级缓存
		mapper = (MajorMapper) sqlSession.getMapper(MajorMapper.class);
		Major result2 =mapper.selectByPrimaryKey(1L);
		assertThat(result2, not(result1));//必然重新检索而认为不是一个对象
		
		sqlSession.close();

	}

}
